const TerserJsPlugin = require('terser-webpack-plugin');
const ESLintPlugin = require('eslint-webpack-plugin');
const glob = require('glob');
const path = require('path');

module.exports = (env, options) => {
  const mode = options.mode;
  const isProduction = mode === 'production';
  const isDevelopment = !isProduction;
  const sourceFiles = glob.sync('js/*.es6.js', {
    ignore: ['js/f1_p13n.local-storage-api.es6.js'],
  });
  const entry = {};
  sourceFiles.forEach(file => {
    const key = path.basename(file, '.es6.js');
    entry[key] = `./${file}`;
  });
  return {
    entry,
    context: __dirname,
    mode,
    optimization: {
      minimizer: [
        new TerserJsPlugin({
          sourceMap: false,
          terserOptions: {
            comments: false,
          },
          extractComments: false,
        }),
      ],
    },
    devtool: isDevelopment ? 'source-map' : false,
    output: {
      path: `${__dirname}/js`,
      filename: '[name].min.js',
    },
    plugins: [
      new ESLintPlugin({
        overrideConfigFile: path.resolve(__dirname, '.eslintrc.js'),
      }),
    ],
    module: {
      rules: [
        {
          test: /\.js?$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
            },
          ],
        },
      ],
    },
  };
};
