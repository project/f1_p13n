import LocalStorageApi from './f1_p13n.local-storage-api.es6.js';

(function () {
  const domReady = callback => {
    const listener = () => {
      callback();
      document.removeEventListener('DOMContentLoaded', listener);
    };
    if (document.readyState !== 'loading') {
      callback();
    } else {
      document.addEventListener('DOMContentLoaded', listener);
    }
  };

  domReady(() => {
    const localStorageApi = new LocalStorageApi();
    // Fetch userModel form localStorage. If no localStorage exists, fethUserModel will return an empty object.
    const userModel = localStorageApi.fetchUserModel();
    // Start searching for meta tags and add count.
    const metaTags = document.querySelectorAll('meta[property^="pn:"]');
    metaTags.forEach(function (tag) {
      // Parse dimension (Ex: pn:dimension).
      const dimension = tag.getAttribute('property').substring(3);
      // If dimension does not exist in userModel, create key with an empty object as the value.
      if (!userModel[dimension]) {
        userModel[dimension] = {};
      }
      // Get content attribute that holds dimension value.
      const content = tag.getAttribute('content').split(',');
      // Dimension values can be comma separated
      content.forEach(function (dimensionValue) {
        // Update dimension value count for respective dimension.
        userModel[dimension][dimensionValue.trim()] =
          userModel[dimension][dimensionValue.trim()] + 1 || 1;
      });
    });
    // Increment the number of times userModel has been updated.
    userModel.updated = userModel.updated + 1 || 1;
    localStorageApi.updateUserModel(userModel);
  });
})();
