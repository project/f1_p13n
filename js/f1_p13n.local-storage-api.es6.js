/**
 * Creates a new object that fetches and updates data in localStorage.
 */
export default class LocalStorageAPI {
  constructor() {
    this.itemName = 'f1_p13n__Model';
  }

  /**
   * Creates a new user model, erasing any data previously stored in the user model in localStoage.
   */
  createNewUserModel() {
    localStorage.setItem(this.itemName, JSON.stringify({}));
  }

  /**
   * Fetches the existing user model form localStorage.
   */
  fetchUserModel() {
    return JSON.parse(localStorage.getItem(this.itemName)) || {};
  }

  /**
   * Fetches the provided dimension and it's respective topics/counts.
   * @param {string} dimension
   */
  fetchInterestTermsForDimension(dimension) {
    const userModel = (JSON.parse(localStorage.getItem(this.itemName)) || {})[
      dimension
    ];

    return userModel;
  }

  /**
   * Fetches the top n topics in a dimension.
   * @param {string} dimension
   * @param {number} n
   */
  fetchDimensionValues(dimension, n = 1) {
    const selectedDimension = this.fetchInterestTermsForDimension(dimension);
    const maxTopics = new Map();

    if (selectedDimension) {
      const allDimensionValues = Object.keys(selectedDimension).sort(
        (a, b) => selectedDimension[b] - selectedDimension[a]
      );
      if (n > Object.keys(selectedDimension).length) {
        return allDimensionValues;
      }

      let count = 0;
      // TODO: refactor to handle dimension:count
      // audience:2, returns top 2 values. If there are ties, return all
      // If there are 2 ties for first and 4 ties for second, return all 6
      allDimensionValues.forEach((key, index) => {
        if (index < n || selectedDimension[key] === count) {
          count = selectedDimension[key];
          maxTopics.set(key, selectedDimension[key]);
        }
      });
    }

    return maxTopics;
  }

  /**
   * Accepts an object of dimensions, topics, and topic counts.
   * Updates localStorage with the new object.
   * @param {LocalStorageApi} userModel
   */
  updateUserModel(userModel) {
    localStorage.setItem(this.itemName, JSON.stringify(userModel));

    return this;
  }
}
