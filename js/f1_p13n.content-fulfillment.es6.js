/* eslint no-console: ["error", { allow: ["error"] }] */
import 'core-js/stable';
import 'regenerator-runtime/runtime';

import base64url from 'base64-url';
import LocalStorageApi from './f1_p13n.local-storage-api.es6.js';

document.addEventListener('DOMContentLoaded', async function () {
  // Start searching for meta tags and add count.
  const emptyContent = document.querySelectorAll('div.p13n-experience');
  const p13nData = {};
  emptyContent.forEach(element => {
    if ('pid' in element.dataset) {
      const id = element.dataset.pid;
      // Get experience id.
      p13nData[id] = {};
      const currentUserlocalStorage = new LocalStorageApi();

      let dimensions = [];
      // Get dimensions for each empty p13n div.
      if (element.dataset.pid && element.dataset.dimensions) {
        dimensions = element.dataset.dimensions.split(' ');
      }
      dimensions.forEach(dimension => {
        // fetchDimensionValues may return a multivalue map in case of ties.
        const topDimensionValue = Array.from(
          currentUserlocalStorage.fetchDimensionValues(dimension).keys()
        );
        p13nData[id][dimension] = topDimensionValue;
      });
    }
  });
  const baseUrl = window.location.origin;
  const endpoint = `${baseUrl}/personalization/content/${base64url.encode(
    JSON.stringify(p13nData)
  )}`;
  // Send request to Drupal controller/endpoint.
  fetch(endpoint, {
    headers: { 'Content-Type': 'application/json; charset=utf-8' },
    method: 'POST',
  })
    .then(response => response.json())
    .then(data => {
      Object.keys(data).forEach(fufilledP13nExperienceId => {
        // If fulfillment status is successful, get respective empty p13n div.
        if (data[fufilledP13nExperienceId].status === 200) {
          const emptyP13nDiv = document.querySelector(
            `[data-pid=${fufilledP13nExperienceId}]`
          );
          // Get markup from request.
          const markup = data[fufilledP13nExperienceId].markupContent;
          // Get any existing markup in the empty p13n div.
          const existingContent = emptyP13nDiv.innerHTML;
          switch (data[fufilledP13nExperienceId].action) {
            case 'replace':
              emptyP13nDiv.innerHTML = markup;
              break;
            case 'prepend':
              emptyP13nDiv.innerHTML = markup + existingContent;
              break;
            case 'append':
              emptyP13nDiv.innerHTML = existingContent + markup;
              break;
          }
          // Add new class signifiying p13n div has been fulfilled.
          emptyP13nDiv.classList.remove('p13n-fulfill');
          emptyP13nDiv.classList.add('p13n-fulfilled');
          // Fire event to announce to the world that p13n div has been fulfilled.
          const fulfillEvent = new CustomEvent('p13n-fulfilled', {
            detail: {
              action: data[fufilledP13nExperienceId].action,
            },
            bubbles: true,
          });
          emptyP13nDiv.dispatchEvent(fulfillEvent);
        }
      });
    })
    .catch(error => console.error(error, 'Error'));
});
