<?php

namespace Drupal\f1_p13n\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityPublishedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\f1_p13n\CampaignInterface;

/**
 * Defines the Campaign entity.
 *
 * @ingroup f1_p13n
 *
 * @ContentEntityType(
 *   id = "f1_p13n_campaign",
 *   label = @Translation("Campaign"),
 *   label_collection = @Translation("Campaigns"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\f1_p13n\CampaignListBuilder",
 *     "views_data" = "Drupal\f1_p13n\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\f1_p13n\Form\CampaignForm",
 *       "add" = "Drupal\f1_p13n\Form\CampaignForm",
 *       "edit" = "Drupal\f1_p13n\Form\CampaignForm",
 *       "delete" = "Drupal\f1_p13n\Form\CampaignDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   base_table = "f1_p13n_campaign",
 *   data_table = "f1_p13n_campaign_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer f1_p13n",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/personalization/campaigns/add",
 *     "edit-form" = "/admin/structure/personalization/campaigns/{f1_p13n_campaign}/edit",
 *     "delete-form" = "/admin/structure/personalization/campaigns/{f1_p13n_campaign}/delete",
 *     "collection" = "/admin/structure/personalization/campaigns",
 *   }
 * )
 */
class Campaign extends ContentEntityBase implements CampaignInterface {

  use EntityPublishedTrait;

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created) {
    return $this->set('created', (int) $created);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Campaign title'))
      ->setDescription(t('The title of the personalization campaign for administrative purposes.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -100,
      ])
      ->setRequired(TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Active'))
      ->setDescription(t("Indicates whether the campaign's experiences are to be displayed. Note that an experience may be displayed by multiple campaigns."))
      ->setDefaultValue(TRUE)
      ->setSettings(['on_label' => 'Active', 'off_label' => 'Inactive'])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -99,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The date when the campaign was created.'));

    return $fields;
  }

}
