<?php

namespace Drupal\f1_p13n\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\f1_p13n\Event\ExperienceEvent;
use Drupal\f1_p13n\Event\ExperienceEvents;
use Drupal\f1_p13n\ExperienceInterface;

/**
 * Defines the Experience entity.
 *
 * @ingroup f1_p13n
 *
 * @ContentEntityType(
 *   id = "f1_p13n_experience",
 *   label = @Translation("Experience"),
 *   handlers = {
 *     "storage" = "Drupal\f1_p13n\ExperienceStorage",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\f1_p13n\ExperienceListBuilder",
 *     "views_data" = "Drupal\f1_p13n\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\f1_p13n\Form\ExperienceForm",
 *       "add" = "Drupal\f1_p13n\Form\ExperienceForm",
 *       "edit" = "Drupal\f1_p13n\Form\ExperienceForm",
 *       "delete" = "Drupal\f1_p13n\Form\ExperienceDeleteForm",
 *     },
 *     "inline_form" = "Drupal\f1_p13n\Form\ExperienceInlineForm",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider"
 *     },
 *   },
 *   base_table = "f1_p13n_experience",
 *   data_table = "f1_p13n_experience_field_data",
 *   revision_table = "f1_p13n_experience_revision",
 *   revision_data_table = "f1_p13n_experience_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer f1_p13n",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *     "published" = "status",
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/personalization/experiences/add",
 *     "edit-form" = "/admin/structure/personalization/experiences/{f1_p13n_experience}/edit",
 *     "delete-form" = "/admin/structure/personalization/experiences/{f1_p13n_experience}/delete",
 *     "collection" = "/admin/structure/personalization/experiences",
 *   }
 * )
 */
class Experience extends ContentEntityBase implements ExperienceInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * Check if experience is in active campaign.
   *
   * @return bool
   *   TRUE if experience is in active experience.
   *
   * @todo Create getter for campaigns.
   */
  public function hasActiveCampaign() {
    $campaigns = $this->get('campaigns')->referencedEntities();
    foreach ($campaigns as $campaign) {
      if ($campaign->isPublished()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Experience name'))
      ->setDescription(t('Specify a name to identify this experience, used for site administration.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setRequired(TRUE);

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Short description of the personalization experience, used for site administration.'))
      ->setRevisionable(TRUE)
      ->setSettings([
        'max_length' => 255,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -1,
      ])
      ->setRequired(FALSE);

    $fields['campaigns'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Campaign(s)'))
      ->setDescription(t('Assign this experience to one or more campaigns. Required.'))
      ->setSetting('target_type', 'f1_p13n_campaign')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setRequired(TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['rules'] = BaseFieldDefinition::create('f1_p13n_rules')
      ->setLabel(t('Conditions & Reactions'))
      ->setDescription(t('Set of condition/reactions for the experience.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    // Call the parent's presave method to perform validate and calculate dependencies.
    parent::preSave($storage);

    // Create a new ExperienceEvent object and dispatch the PRESAVE event.
    $event = new ExperienceEvent($this);
    $event_dispatcher = \Drupal::service('event_dispatcher');
    $event_dispatcher->dispatch(ExperienceEvents::PRESAVE, $event);
  }

  /**
   * Get the list of rules associated with an experience.
   *
   * @return array
   *   Returns an array of rules as an associative array keyed by rule UUID.
   */
  public function getRules() {
    $rules = [];
    $rule_items = $this->rules->value ?? [];

    foreach ($rule_items as $rule_item) {
      $rules[$rule_item['uuid']] = $rule_item;
    }

    return $rules;
  }

}
