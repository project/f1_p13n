<?php

namespace Drupal\f1_p13n\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a condition plugin annotation object for personalization.
 *
 * Personalization uses conditions to determine reactions if any.
 *
 * Plugin Namespace: Plugin\f1_p13n\Condition
 *
 * @see \Drupal\f1_p13n\ExperiencePluginManager
 * @see \Drupal\f1_p13n\Condition\ConditionInterface
 * @see plugin_api
 *
 * @todo Use annotation base for this and reaction.
 *
 * @Annotation
 */
class Condition extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The category under which the condition is listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

  /**
   * The dimension associated with this condition.
   *
   * @var string
   */
  public $dimension;

}
