<?php

namespace Drupal\f1_p13n\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a reaction plugin annotation object for personalization.
 *
 * Personalization uses reactions to render content based on conditions.
 *
 * Plugin Namespace: Plugin\f1_p13n\Reaction
 *
 * @see \Drupal\f1_p13n\ExperiencePluginManager
 * @see \Drupal\f1_p13n\Reaction\ReactionInterface
 * @see plugin_api
 *
 * @todo Use annotation base for this and condition.
 *
 * @Annotation
 */
class Reaction extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The category under which the reaction is listed in the UI.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $category;

  /**
   * The dimension associated with this condition.
   *
   * @var string
   */
  public $dimension;

}
