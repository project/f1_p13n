<?php

namespace Drupal\f1_p13n;

use DateTime;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Campaign entities.
 *
 * @ingroup f1_p13n
 */
class CampaignListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();
    $build['table']['#empty'] = $this->t('No personalization campaigns have been created.');
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // It may be useful to display the campaign ID at some point in the future.
    // $header['id'] = $this->t('ID');

    $header['title'] = $this->t('Campaign title');
    $header['created'] = $this->t('Created');
    $header['status'] = $this->t('Status');
    $header['experiences'] = $this->t('Experiences');

    // Operations list is added in parent.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\f1_p13n\Entity\Campaign $entity */

    // It may be useful to display the campaign ID at some point in the future.
    // $row['id'] = $entity->id();

    $row['title'] = Link::createFromRoute(
      $entity->label(),
      'entity.f1_p13n_campaign.edit_form',
      ['f1_p13n_campaign' => $entity->id()]
    );

    $created = $entity->getCreated();
    $date = new DateTime("@$created");
    $row['created'] = $date->format('F j, Y');

    // @todo pull display values from field definition settings.
    $row['status'] = $entity->get('status')->value ? $this->t('Active') : $this->t('Inactive');

    // Display an unordered (but sorted by experience id) list of experiences.
    $experiences_list = views_embed_view('experiences', 'campaign_list', $entity->id());

    // @todo: DI the renderer.
    $row['experiences'] = \Drupal::service('renderer')->renderPlain($experiences_list);

    return $row + parent::buildRow($entity);
  }

}
