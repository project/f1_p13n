<?php

namespace Drupal\f1_p13n\Condition;

/**
 * Defines an interface for personalization plugins.
 */
interface ConditionInterface {

  /**
   * Evaluate the condition.
   *
   * @param array $rule
   *   Condition settings and reaction for rule.
   * @param array $experience_dimensions
   *   Experience dimensions from local storage.
   *
   * @return bool
   *   TRUE if condition passes.
   */
  public function evaluate(array $rule, array $experience_dimensions);

}
