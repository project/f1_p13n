<?php

namespace Drupal\f1_p13n\Condition;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provides base class for personalization conditions.
 *
 * @todo Combine with reaction plugin and use PluginWithFormsInterface.
 *
 * @ingroup plugin_api
 */
abstract class ConditionPluginBase extends PluginBase implements ConditionInterface, ConfigurableInterface, PluginFormInterface {

  /**
   * Constructs a ConditionBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->setConfiguration($configuration);
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration) {
    $this->configuration = $configuration + $this->defaultConfiguration();

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return [
      'id' => $this->getPluginId(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => $this->getPluginId(),
    ];
  }

  /**
   * {@inheritdoc}
   *
   * @todo Make this method more generic. This may have more impact as a trait?
   */
  public function evaluate(array $rule, array $experience_dimensions) {
    $dimension = $rule['condition']['settings']['dimension'];
    $any_value = $rule['condition']['settings']['any_value'];
    // If dimension from rule exists in experience_dimensions.
    if(!empty($experience_dimensions[$dimension])) {
      if($any_value) {
        return true;
      }
      else {
        // Get specified dimension value from rule.
        $dimension_value = $rule['condition']['settings']['dimension_value'];
        // Get top dimension values for dimension in experience_dimensions.
        $top_values = $experience_dimensions[$dimension];
        // If value(s) exists and specified dimension value exists in top values for respective dimenion.
        if(!empty($dimension_value) && in_array($dimension_value, $top_values)) {
          return true;
        }
        // If it does not exist in top values, return false.
        else {
          return false;
        }
      }
    }
    return false;
  }

}
