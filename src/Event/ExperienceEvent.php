<?php

namespace Drupal\f1_p13n\Event;

use Symfony\Component\EventDispatcher\Event;
use Drupal\f1_p13n\Entity\Experience;

/**
 * Defines a event class for handling Experience events such as CRUD operations.
 *
 * @see \Drupal\f1_p13n\Entity\Experience
 */
class ExperienceEvent extends Event {

  /**
   * The experience entity.
   *
   * @var \Drupal\f1_p13n\Entity\Experience
   */
  protected $experience;

  /**
   * Constructs a ExperienceEvent.
   *
   * @param \Drupal\f1_p13n\Entity\Experience $experience
   *   The experience entity.
   */
  public function __construct(Experience $experience) {
    $this->experience = $experience;
  }

  /**
   * Returns the experience that is undergoing some event operation.
   *
   * @return \Drupal\f1_p13n\Entity\Experience
   *   The experience entity.
   */
  public function getExperience() {
    return $this->experience;
  }

}
