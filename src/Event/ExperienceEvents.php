<?php

namespace Drupal\f1_p13n\Event;

/**
 * Contains all events dispatched by personalization.
 */
final class ExperienceEvents {

  /**
   * The event triggered before an experience is saved.
   *
   * This event allows rules, reactions, and conditions to manage any entities
   * or storage used in the experience, for example paragraph-based content
   * created by reactions.
   *
   * @Event
   *
   * @see \Drupal\f1_p13n\Event\ExperienceEvent
   *
   * @var string
   */
  const PRESAVE = 'experience.presave';

  /**
   * The event triggered before an experience is deleted.
   *
   * This event allows rules, reactions, and conditions to manage any entities
   * or storage used in the experience, for example paragraph-based content
   * created by reactions.
   *
   * @Event
   *
   * @see \Drupal\f1_p13n\Event\ExperienceEvent
   *
   * @todo implement.
   *
   * @var string
   */
  const DELETE = 'experience.delete';

}
