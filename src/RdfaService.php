<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslationInterface;

/**
 * A class containing methods for "RDFa" services.
 */
class RdfaService {
  use StringTranslationTrait;

  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * The menu active trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrail
   */
  protected $menuActiveTrail;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vocabStorage;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Dimensions settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Constructs a new RdfaService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The menu active trail service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The config factory service.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityRepositoryInterface $entity_repository,
    LanguageManagerInterface $language_manager,
    MenuActiveTrailInterface $menu_active_trail,
    ConfigFactoryInterface $config_factory,
    PathMatcherInterface $path_matcher,
    TranslationInterface $string_translation
  ) {

    $this->vocabStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');
    $this->entityRepository = $entity_repository;
    $this->languageManager = $language_manager;
    $this->menuActiveTrail = $menu_active_trail;
    $this->configFactory = $config_factory;
    $this->pathMatcher = $path_matcher;
    $this->stringTranslation = $string_translation;
  }

  /**
   * Get target vocabulary reference fields.
   *
   * This returns an array of vocabulary term names that are referenced
   * by any field in the context entity. Returns the value 'all' if no
   * terms are found in the context entity for a target vocabulary optionally.
   *
   * @param array $target_vids
   *   An array of target vocabulary IDs.
   * @param mixed $context_entity
   *   An entity with fields, could be a node, a term, a media item, etc.
   * @param array $target_types
   *   An array of filtered target types, if empty then all are returned.
   *
   * @return array
   *   Array of term IDs for the terms that signify an event is live.
   */
  public function getTargetVocabularyRefFields(array $target_vids, $context_entity, array $target_types) {
    $target_fields = [];

    // Get the field definitions associated with the context entity.
    $context_fields = $context_entity->getFieldDefinitions();
    foreach ($context_fields as $field) {
      if ($field instanceof FieldConfig) {
        // Only process the entity_reference fields, but this will return a
        // list of ER fields referring to any type of entity such as media,
        // taxonomy vocab, etc.
        if ($field->getType() == 'entity_reference') {
          // Make sure reference target type (node, taxonomy_term) is one we are
          // looking for.
          if (empty($target_types) || in_array($field->getSetting('target_type'), $target_types)) {
            // Retrieve the target bundles as an array of machine IDs of
            // targets.
            $target_bundles = $field->getSettings()['handler_settings']['target_bundles'];

            // See if each target_vocabulary is populated by the target field.
            // If it is, we'll use that field later to provide values for the
            // entity reference.
            foreach ($target_vids as $target_id) {
              if (in_array($target_id, $target_bundles)) {
                // Note that a single ER field could refer to multiple
                // taxonomies. And a single taxonomy could be referred to by
                // multiple fields.
                $target_fields[$target_id][] = $field->getName();
              }
            }
          }
        }
      }
    }

    return $target_fields;
  }

  /**
   * Get entity terms names.
   *
   * This returns an array of vocabulary term names that are referenced
   * by any field in the context entity. Returns the value 'all' if no
   * terms are found in the context entity for a target vocabulary optionally.
   *
   * @param array $target_vids
   *   An array of target vocabulary IDs.
   * @param mixed $context_entity
   *   An entity with fields, could be a node, a term, a media item, etc.
   * @param bool $label
   *   Return term as TIDS (if FALSE) or names (if TRUE).
   *
   * @return array
   *   Array of term IDs for the terms that signify an event is live.
   */
  public function getEntityRelatedTerms(array $target_vids, $context_entity, $label = TRUE) {
    $filter_array = array_fill_keys(array_values($target_vids), []);
    $target_fields = $this->getTargetVocabularyRefFields($target_vids, $context_entity, ['taxonomy_term']);

    foreach ($target_vids as $target_vid) {
      if (isset($target_fields[$target_vid])) {
        foreach ($target_fields[$target_vid] as $field) {
          // Retrieve the referenced entities for the field and process as
          // taxonomy terms.
          $taxonomy_terms = $context_entity->get($field)->referencedEntities();

          /** @var \Drupal\taxonomy\TermInterface $taxonomy_term */
          foreach ($taxonomy_terms as $taxonomy_term) {
            $bundle = $taxonomy_term->bundle();
            // Check to make sure the bundle of the found term is one that we're
            // generating contextual values for.
            if (in_array($bundle, $target_vids)) {
              // If it is, use the target_vids array to record values.
              $term_addition = $label ? $taxonomy_term->label() : $taxonomy_term->id();

              // Only add to the filter array for this bundle if the term
              // hasn't been added yet.
              if (!in_array($term_addition, $filter_array[$bundle])) {
                $filter_array[$bundle][] = $term_addition;
              }
            }
          }
        }
      }
    }

    return $filter_array;
  }

  /**
   * Create argument list of taxonomy terms or tids.
   *
   * This function creates an argument list for views or other consumers
   * of the form [TERM]+[TERM] with spaces optionally replaced by
   * dashes and the empty list for a taxonomy replaced by 'all' for
   * views contextual filters.
   *
   * @param array $filter_array
   *   An array of target vocabulary IDs and associated terms.
   * @param array $options
   *   Options for creating the argument list.
   *
   * @return array
   *   Array of term IDs for the terms that signify an event is live.
   */
  public function createArgList(array $filter_array, array $options = []) {
    $default_options = [
      'separator' => $options['separator'] ?? '+',
      'dash_replace' => $options['dash_replace'] ?? TRUE,
      // Send empty_string = NULL to avoid replacing.
      'empty_string' => $options['empty_string'] ?? 'all',
    ];

    $filter_values = [];

    // Return the filter targets for embedding in the drupal_view call.
    // Multiple targets are separated by plus signs.
    foreach ($filter_array as $vid => $value_array) {
      if (empty($value_array)) {
        if ($default_options['empty_string']) {
          $filter_values[] = 'all';
        }
      }
      else {
        // Unless dash replace was specifically not requested, convert spaces
        // to dashes.
        if ($default_options['dash_replace']) {
          array_walk($value_array, function (&$value, &$key) {
            $value = str_replace(' ', '-', $value);
          });
        }

        // Implode the value array into a string using the separator.
        $filter_values[$vid] = implode($default_options['separator'], $value_array);
      }
    }

    return $filter_values;
  }

  /**
   * Get the dimensions (list of vocabularies) in use for discovery.
   *
   * @return array
   *   The list of dimensions.
   */
  public function getTaxonomyDimensions() {
    // Get the list of vocabulary dimensions intended to be used.
    $vocabularies_config = $this->configFactory
      ->get('f1_p13n.settings')
      ->get('taxonomy_dimensions') ?? [];

    if (!empty($vocabularies_config)) {
      // Get the list of vocabularies currently defined on the site.
      $vocabularies = $this->vocabStorage->loadMultiple();

      // Check to see whether the dimension is defined as a vocabulary on the site.
      // It is possible that a vocabulary was selected for discovery and saved into the configuration list
      // and then the vocabulary was deleted.
      foreach ($vocabularies_config as $vid) {
        if (!isset($vocabularies[$vid])) {
          unset($vocabularies_config[$vid]);
        }
      }
    }

    return $vocabularies_config;
  }

  /**
   * Return the label of the section.
   *
   * The top level menu item in the active trail for this menu item.
   *
   * @param string $menu
   *   The menu name.
   * @param bool $frontpage
   *   Whether to include the frontpage as a separate section.
   *
   * @return array
   *   The active trail ids.
   */
  public function getContentSectionLabels($menu, $frontpage = TRUE) {
    $section_labels = [];
    // Check if the front page and whether to include front page.
    if ($frontpage && $this->pathMatcher->isFrontPage()) {
      $section_labels[] = $this->t("Homepage");
    }
    else {
      $active_trail_ids = array_reverse($this->menuActiveTrail->getActiveTrailIds($menu));
      // Shift off the first element, which appears to always be empty.
      array_shift($active_trail_ids);
      // Shift off again to get the top of the menu.
      $top_menu_item = array_shift($active_trail_ids);
      // Split apart, load the menu, and extract the label.
      $type_uuid = explode(':', $top_menu_item);

      if (count($type_uuid) === 2 && $type_uuid[0]) {
        // Load the menu link entity.
        $menu_link = $this->entityRepository->loadEntityByUuid($type_uuid[0], $type_uuid[1]);
        $section_labels[] = $menu_link->label();
      }
    }

    return $section_labels;
  }

}
