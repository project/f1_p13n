<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Experience entities.
 *
 * @ingroup f1_p13n
 */
interface ExperienceInterface extends ContentEntityInterface {

  /**
   * Gets the Experience name.
   *
   * @return string
   *   Name of the Experience.
   */
  public function getName();

  /**
   * Sets the Experience name.
   *
   * @param string $name
   *   The Experience name.
   *
   * @return \Drupal\f1_p13n\ExperienceInterface
   *   The called Experience entity.
   */
  public function setName($name);

}
