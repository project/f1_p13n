<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines an interface for experience entity storage classes.
 */
interface ExperienceStorageInterface extends ContentEntityStorageInterface {

  /**
   * Returns a array of experiences given UUID.
   *
   * @param string $uuid
   *   The field to use to key the returned associative array.
   *
   * @return object
   *   An experience object matching the UUID or NULL if not found.
   */
  public function loadByUuid($uuid);

  /**
   * Returns a array of experiences keyed by entity ID or a custom field.
   *
   * @param string $key_field
   *   The field to use to key the returned associative array.
   *   NULL is the default, entity id.
   *
   * @return array
   *   Array of experiences keyed either by entity ID (if key_field is NULL) or
   *   by a particular field key such as uuid.
   */
  public function loadWithKey($key_field);

}
