<?php

namespace Drupal\f1_p13n\Form;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Experience edit forms.
 *
 * @ingroup f1_p13n
 *
 * @todo Use subforms for condition and reaction settings.
 * Use a uniquely generated rule id or continue using delta.
 * Potentially organize rules field around custom element and or widget.
 * Remove unused form variable from setRulesValue.
 * Modernize ajax calls.
 */
class ExperienceForm extends ContentEntityForm {

  /**
   * The current user account.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $account;

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $conditionManager;

  /**
   * The reaction plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $reactionManager;

  /**
   * The UUID service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    $instance = parent::create($container);
    $instance->account = $container->get('current_user');
    $instance->conditionManager = $container->get('plugin.manager.f1_p13n.condition');
    $instance->reactionManager = $container->get('plugin.manager.f1_p13n.reaction');
    $instance->uuidService = $container->get('uuid');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var \Drupal\f1_p13n\Entity\Experience $entity */
    $form = parent::buildForm($form, $form_state);

    $form['rules_settings'] = [
      '#type' => 'fieldset',
      '#attributes' => [
        'id' => 'rules_settings',
        'class' => [
          'rules-settings-wrapper',
        ],
      ],
      '#title' => $this->t('Configure Rules'),
      '#description' => $this->t('The set of rules (conditions and reactions) for the experience.'),
      '#weight' => 10,
    ];

    $form['rules_settings']['rules'] = [
      '#type' => 'table',
      '#header' => ['', '', '', '', ''],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
      '#caption' => $this->t('Specify the personalization rules for this experience. Rules can be reordered.'),
    ];

    $trigger = $form_state->getTriggeringElement();
    if (!empty($trigger['#op'])
        && ($trigger['#op'] == 'select_condition'
          || $trigger['#op'] == 'select_reaction')
    ) {
      $parents = array_slice($trigger['#parents'], 0, -3);
      $updated_rules = NestedArray::getValue($form_state->getUserInput(), $parents);
      $this->setRulesValue($form_state, $updated_rules);
    }

    $rules = $form_state->get('rules') ?? $this->entity->rules->value;

    if (!empty($rules)) {
      foreach ($rules as $delta => $rule) {
        $form['rules_settings']['rules'][$delta] = $this->setRuleFormElement($form_state, $delta, $rule);
      }
    }

    $form['rules_settings']['add_rule'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Rule'),
      '#op' => 'add_rule',
      '#name' => 'add_rule',
      '#submit' => [[$this, 'addRuleElement']],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => [$this, 'updateRulesAjax'],
        'wrapper' => 'rules_settings',
      ],
    ];

    return $form;
  }

  /**
   * Provides callback for adding rule.
   *
   * @param array $form
   *   The form of the entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   */
  public function addRuleElement(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#parents'], 0, -1);
    $parents[] = 'rules';

    $rules = NestedArray::getValue($form_state->getUserInput(), $parents) ?? [];
    $weight = 0;
    if (!empty($rules)) {
      uasort($rules, [SortArray::class, 'sortByWeightElement']);
      $last_rule = end($rules);
      reset($rules);
      $weight = $last_rule['weight'] + 1;
    }

    // Create a new, empty rule and assign it a generated UUID.
    $rules[] = [
      'uuid' => $this->uuidService->generate(),
      'condition' => [
        'id' => '',
      ],
      'reaction' => [
        'id' => '',
      ],
      'weight' => $weight,
    ];

    $this->setRulesValue($form_state, $rules);
  }

  /**
   * Helper method to set rule form fields.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   * @param int $delta
   *   The delta of the rule.
   * @param array $rule
   *   Optional rule to set.
   */
  protected function setRuleFormElement(FormStateInterface $form_state, $delta, array $rule = []) {
    $element = [];

    // Stash the rule's UUID in a hidden field to carry it along through rule
    // creation and editing. Note that the field is "uuid" and not "id" so that
    // there's no namespace collision with form ids, etc. Every rule that is
    // created has an assigned UUID.
    $element['uuid'] = [
      '#type' => 'hidden',
      '#default_value' => $rule['uuid'] ?? $this->uuidService->generate(),
    ];

    // Retrieve the rule weight, or use a zero weight if none.
    $weight = $rule['weight'] ?? 0;
    $element['#weight'] = $weight;

    // Set attributes to allow the rule to be dragged to change its weight.
    $element['#attributes']['class'][] = 'draggable';

    // Identify the triggering action.
    $trigger = $form_state->getTriggeringElement();

    // Get condition plugin info.
    $condition_plugin_info = $this->getPluginConfigurationInfo('condition', $trigger, $delta, $rule, $form_state);

    // Set settings form element if component not found.
    $settings_not_found = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'index-changed',
          'messages',
          'messages--error',
        ],
      ],
      '#children' => $this->t("Component was not found or could not be created."),
    ];

    // Set condition id default value.
    $condition_id = !empty($condition_plugin_info['id']) ? $condition_plugin_info['id'] : '_none';

    // Set condition form element.
    $element['condition'] = [
      '#type' => 'container',
      'id' => [
        '#title' => $this->t('Condition'),
        '#title_display' => 'before',
        '#type' => 'select',
        '#options' => $this->getConditionOptions(),
        '#default_value' => $condition_id,
        '#op' => 'select_condition',
        '#description' => $this->t("Select the condition to trigger display of this rule."),
        '#ajax' => [
          'callback' => [$this, 'updateRulesAjax'],
          'wrapper' => 'rules_settings',
        ],
      ],
    ];
    if (!$this->conditionManager->hasDefinition($condition_id) && $condition_id != '_none') {
      $element['condition']['settings'] = $settings_not_found;
    }
    elseif ($condition_id != '_none') {
      $element['condition']['settings'] = $this->getPluginConfigurationForm('condition', $condition_plugin_info['id'], $condition_plugin_info['settings'], $form_state);
    }

    // Get reaction plugin info.
    $reaction_plugin_info = $this->getPluginConfigurationInfo('reaction', $trigger, $delta, $rule, $form_state);

    // Set reaction id default value.
    $reaction_id = !empty($reaction_plugin_info['id']) ? $reaction_plugin_info['id'] : '_none';

    // Set reaction form element.
    $element['reaction'] = [
      '#type' => 'container',
      'id' => [
        '#title' => $this->t('Reaction'),
        '#title_display' => 'before',
        '#type' => 'select',
        '#options' => $this->getReactionOptions(),
        '#default_value' => $reaction_id,
        '#op' => 'select_reaction',
        '#description' => $this->t("Select the reaction to use when this rule is selected."),
        '#ajax' => [
          'callback' => [$this, 'updateRulesAjax'],
          'wrapper' => 'rules_settings',
        ],
      ],
    ];

    // Set reaction settings form.
    if (!$this->reactionManager->hasDefinition($reaction_id) && $reaction_id != '_none') {
      $element['reaction']['settings'] = $settings_not_found;
    }
    elseif ($reaction_id != '_none') {
      $element['reaction']['settings'] = $this->getPluginConfigurationForm('reaction', $reaction_plugin_info['id'], $reaction_plugin_info['settings'], $form_state);
    }

    $element['remove'] = [
      '#type' => 'submit',
      '#value' => $this->t('Remove'),
      '#op' => 'remove_rule',
      '#name' => 'remove_rule_' . $delta,
      '#submit' => [[$this, 'removeRuleElement']],
      '#ajax' => [
        'callback' => [$this, 'updateRulesAjax'],
        'wrapper' => 'rules_settings',
      ],
    ];

    $element['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $weight,
      '#attributes' => [
        'class' => ['table-sort-weight'],
      ],
      '#delta' => 10,
    ];

    return $element;
  }

  /**
   * Helper method to set condition or reaction configuration form.
   *
   * @param string $type
   *   Condition or reaction plugin.
   * @param string $plugin_id
   *   The id of the condition or reaction.
   * @param array $settings
   *   The plugin configuration values.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @return array
   *   The form structure.
   */
  protected function getPluginConfigurationForm($type, $plugin_id, array $settings, FormStateInterface $form_state) {
    $plugin_manager = $type == 'condition' ? $this->conditionManager : $this->reactionManager;
    $plugin = $plugin_manager->createInstance($plugin_id, $settings);
    return $plugin->buildConfigurationForm([], $form_state);
  }

  /**
   * Helper method to set plugin id and settings.
   *
   * @param string $type
   *   Condition or reaction plugin.
   * @param array|null $trigger
   *   The triggering element or null.
   * @param int $delta
   *   The rule delta.
   * @param array $rule
   *   The rule being set.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @return array
   *   Array consisting of id and settings.
   */
  protected function getPluginConfigurationInfo($type, $trigger, $delta, array $rule, FormStateInterface $form_state) {
    $trigger_delta = $trigger['#parents'][1] ?? NULL;
    $trigger_op = 'select_' . $type;

    // Plugin id.
    if (!empty($trigger['#op'])
        && $trigger['#op'] == $trigger_op
        && !empty($trigger_delta === $delta)
    ) {
      $plugin_id = $form_state->getValue($trigger['#parents'], '');
    }
    else {
      if (!empty($trigger['#op'])
          && ($trigger['#op'] == 'select_condition'
          || $trigger['#op'] == 'select_reaction')
      ) {
        $element_parents = array_slice($trigger['#parents'], 0, -3);
        array_push($element_parents, $delta, $type, 'id');
        $plugin_id = NestedArray::getValue($form_state->getUserInput(), $element_parents) ?? '';
      }
      else {
        $plugin_id = $rule[$type]['id'] ?? '';
        $plugin_settings = $rule[$type]['settings'] ?? [];
      }
    }

    return [
      'id' => $plugin_id,
      'settings' => $plugin_settings ?? [],
    ];
  }

  /**
   * Provides callback for removing rule.
   *
   * @param array $form
   *   The form of the entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @todo Use relateive parents.
   */
  public function removeRuleElement(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $parents = array_slice($trigger['#parents'], 0, -2);

    $rules = NestedArray::getValue($form_state->getUserInput(), $parents);
    $delta = $trigger['#array_parents'][2];
    unset($rules[$delta]);

    if (!empty($rules)) {
      uasort($rules, [SortArray::class, 'sortByWeightElement']);
    }

    $this->setRulesValue($form_state, $rules);
  }

  /**
   * Provides an ajax callback for updates to rules.
   *
   * @param array $form
   *   The form of the entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   */
  public function updateRulesAjax(array &$form, FormStateInterface $form_state) {
    // Find 'rules_settings'.
    $trigger = $form_state->getTriggeringElement();
    $length = -1;
    if (!empty($trigger['#op'])) {
      switch ($trigger['#op']) {
        case 'remove_rule':
          $length = -3;
          break;

        case 'select_condition':
        case 'select_reaction':
          $length = -4;
          break;
      }
    }

    $parents = array_slice($trigger['#array_parents'], 0, $length);

    return NestedArray::getValue($form, $parents);
  }

  /**
   * Helper to set rules values on entity.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   * @param array $rules
   *   The current rules input values.
   */
  protected function setRulesValue(FormStateInterface $form_state, array $rules = []) {
    $form_state->set('rules', $rules);
    $form_state->setRebuild();
  }

  /**
   * Helper method to get condition options.
   */
  protected function getConditionOptions() {
    $options = [
      '_none' => $this->t('- None -'),
    ];
    $condition_definitions = $this->conditionManager->getGroupedDefinitions();
    foreach ($condition_definitions as $category => $definitions) {
      foreach ($definitions as $definition_id => $definition) {
        $options[$category][$definition_id] = $definition['label'];
      }
    }
    return $options;
  }

  /**
   * Helper method to get reaction options.
   */
  protected function getReactionOptions() {
    $options = [
      '_none' => $this->t('- None -'),
    ];
    $reaction_definitions = $this->reactionManager->getGroupedDefinitions();
    foreach ($reaction_definitions as $category => $definitions) {
      foreach ($definitions as $definition_id => $definition) {
        $options[$category][$definition_id] = $definition['label'];
      }
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $form_values = $form_state->getValue($form['#parents']);
    if ($rules = $form_values['rules']) {
      uasort($rules, [SortArray::class, 'sortByWeightElement']);
      $this->entity->rules->value = $rules;
      foreach ($rules as $delta => $rule) {
        $this->executeRuleHandlers($rule, 'submit', $delta, $form, $form_state);
      }
      $form_state->set('rules', $rules);
    }
    elseif (!empty($this->entity)) {
      $this->entity->set('rules', NULL);
      $form_state->set('rules', []);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $form_values = $form_state->getValue($form['#parents']);
    if ($rules = $form_values['rules']) {
      foreach ($rules as $delta => $rule) {
        $this->executeRuleHandlers($rule, 'validate', $delta, $form, $form_state);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * Helper to run submission on condition and reaction settings per rule.
   *
   * @param array $rule
   *   The rule containing condition and reaction settings.
   * @param string $action
   *   Perform submit or validate.
   * @param int $delta
   *   The rule position.
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent.
   *
   * @todo Replace delta with rule uuid.
   * Use plugin instance info instead of passing id and settings separately.
   */
  protected function executeRuleHandlers(array $rule, $action, $delta, array &$form, FormStateInterface $form_state) {
    if (!empty($rule['condition']['id'])
        && !empty($rule['condition']['settings'])
        && $this->conditionManager->hasDefinition($rule['condition']['id'])
    ) {
      $this->executePluginHandler('condition', $action, $delta, $rule['condition']['id'], $rule['condition']['settings'], $form, $form_state);
    }

    if (!empty($rule['reaction']['id'])
        && !empty($rule['reaction']['settings'])
        && $this->reactionManager->hasDefinition($rule['reaction']['id'])
    ) {
      $this->executePluginHandler('reaction', $action, $delta, $rule['reaction']['id'], $rule['reaction']['settings'], $form, $form_state);
    }
  }

  /**
   * Helper to run submission on condition and reaction settings per rule.
   *
   * @param string $type
   *   Condition or reaction.
   * @param string $action
   *   Perform submit or validate.
   * @param int $delta
   *   The rule position.
   * @param string $plugin_id
   *   The plugin id for the condition or reaction.
   * @param array $plugin_settings
   *   The configuration settings for the plugin.
   * @param array $form
   *   The form structure.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent.
   *
   * @todo Use plugin instance info as array instead of passing delta, id,
   * and settings separately.
   */
  protected function executePluginHandler($type, $action, $delta, $plugin_id, array $plugin_settings, array &$form, FormStateInterface $form_state) {
    $plugin_manager = $type == 'condition' ? $this->conditionManager : $this->reactionManager;
    $plugin = $plugin_manager->createInstance($plugin_id, $plugin_settings);
    $plugin_form = $plugin->buildConfigurationForm([], $form_state);
    $plugin_form['#parents'] = $form['rules_settings']['rules'][$delta][$type]['settings']['#parents'];

    if ($action == 'submit') {
      $plugin->submitConfigurationForm($plugin_form, SubformState::createForSubform($plugin_form, $form, $form_state));
    }
    else {
      $plugin->validateConfigurationForm($plugin_form, SubformState::createForSubform($plugin_form, $form, $form_state));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Experience.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Experience.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.f1_p13n_experience.collection');
  }

}
