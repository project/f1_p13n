<?php

namespace Drupal\f1_p13n\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for configuration settings.
 *
 * @ingroup f1_p13n
 */
class PersonalizationSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * PersonalizationSettingsForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Config settings name.
   *
   * @var string
   */
  const SETTINGS = 'f1_p13n.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'f1_p13n_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Section of the settings form related to Discovery.
    $form['discovery'] = [
      '#type' => 'details',
      '#title' => $this->t('Discovery'),
      '#open' => TRUE,
    ];

    // Retrieve the list of taxonomy dimensions from configuration.
    $default_vocabularies = $this->config(static::SETTINGS)
      ->get('taxonomy_dimensions');

    // Section of the discovery form that provides the administrator the option
    // of selecting which taxonomies should be used to form RDFa tags.
    $form['discovery']['vocabularies'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the taxonomy vocabularies to track for discovery'),
      '#options' => $this->getVocabularies(),
      '#default_value' => $default_vocabularies ?? [],
    ];

    return $form;
  }

  /**
   * Helper method to get taxonomy vocabulary options.
   *
   * @return array
   *   Associative array of vocabularies.
   */
  private function getVocabularies() {
    $options = [];
    $vocabularies = $this->entityTypeManager
      ->getStorage('taxonomy_vocabulary')
      ->loadMultiple();

    foreach ($vocabularies as $vocabulary) {
      $options[$vocabulary->id()] = $vocabulary->label();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Clear cache for taxonomy discovery.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Create a container for taxonomy bundles and retrieve the selected list of vocabularies.
    $vocabularies = $form_state->getValue('vocabularies');

    // Filter the list of vocabularies to only include selected.
    foreach ($vocabularies as $vid => $vocabulary) {
      if ($vocabulary === 0) {
        unset($vocabularies[$vid]);
      }
    }

    // Save the list of vocabulary machine ids into configuration.
    $this->config(static::SETTINGS)
      ->set('taxonomy_dimensions', array_keys($vocabularies))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
