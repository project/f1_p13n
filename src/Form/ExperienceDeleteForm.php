<?php

namespace Drupal\f1_p13n\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Url;

/**
 * Provides a form for deleting Experience entities.
 *
 * @ingroup f1_p13n
 */
class ExperienceDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  protected function getRedirectUrl() {
    return Url::fromRoute('entity.f1_p13n_campaign.collection');
  }

}
