<?php

namespace Drupal\f1_p13n\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Campaign entities.
 *
 * @ingroup f1_p13n
 */
class CampaignDeleteForm extends ContentEntityDeleteForm {

}
