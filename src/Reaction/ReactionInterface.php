<?php

namespace Drupal\f1_p13n\Reaction;

/**
 * Defines an interface for personalization plugins.
 */
interface ReactionInterface {

  /**
   * Provide display for reaction.
   *
   * @return array
   *   The render array.
   */
  public function view();

}
