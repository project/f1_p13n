<?php

namespace Drupal\f1_p13n\EventSubscriber;

use Drupal\f1_p13n\Event\ExperienceEvent;
use Drupal\f1_p13n\Event\ExperienceEvents;
use Drupal\f1_p13n\ExperiencePluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriptions for rules.
 *
 * @todo Clean up for standards.
 */
class ExperienceRuleSubscriber implements EventSubscriberInterface {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $conditionManager;

  /**
   * The reaction plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $reactionManager;

  /**
   * Constructs a new ExperienceRuleSubscriber.
   *
   * @param \Drupal\f1_p13n\ExperiencePluginManager $condition_manager
   *   The experience condition manager.
   * @param \Drupal\f1_p13n\ExperiencePluginManager $reaction_manager
   *   The experience reaction manager.
   */
  public function __construct(
    ExperiencePluginManager $condition_manager,
    ExperiencePluginManager $reaction_manager
  ) {
    $this->conditionManager = $condition_manager;
    $this->reactionManager = $reaction_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ExperienceEvents::PRESAVE] = ['onExperiencePresave'];

    return $events;
  }

  /**
   * Subscribe to the user login event dispatched.
   *
   * @param \Drupal\f1_p13n\Event\ExperienceEvent $event
   *   The experience event.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function onExperiencePresave(ExperienceEvent $event) {
    /* @var \Drupal\f1_p13n\Entity\Experience $experience. */
    $experience = $event->getExperience();

    // Retrieve rules associated with the this experience as a weighted list.
    $rules = $experience->getRules();

    // Used to maintain a list of the rules as they exist before completion
    // of the Presave operation.
    $original_rules = [];

    // First, process for deletion any original rules that are no longer present in the experience.
    if ($original = $experience->original) {
      // Retrieve the rules associated with the this experience as a weighted list.
      $original_rules = $original->getRules();

      // Check to see if there are rules that have been deleted by matching UUIDs from the original
      // with the current experience rules.
      // @var \Drupal\f1_p13n\Plugin\Field\FieldType $rule
      foreach ($original_rules as $rule_uuid => $original_rule) {
        // If an original rule has been deleted, trigger the onDelete method of the condition & reaction if they exist.
        if (!isset($rules[$rule_uuid])) {
          $original_condition_settings = $original_rule['condition']['settings'] ?? [];
          $original_reaction_settings = $original_rule['reaction']['settings'] ?? [];
          if ($condition = $this->_conditionNew($original_rule['condition']['id'], $original_condition_settings)) {
            $this->_conditionEvent($condition, 'onDelete');
          }
          if ($reaction = $this->_reactionNew($original_rule['reaction']['id'], $original_reaction_settings)) {
            $this->_reactionEvent($reaction, 'onDelete');
          }

          // Remove this rule from later processing.
          unset($original_rules[$rule_uuid]);
        }
      }
    }

    // Next, look to see if there are new rules (either not in the $original or there were no original rules)
    // and look to see if a "new" pre-save rule has a previous version, in which case initiate onChange event.
    // @var \Drupal\f1_p13n\Plugin\Field\FieldType $rule
    foreach ($rules as $rule_uuid => $rule) {
      // If an original rule has been deleted, trigger the onDelete method of the condition & reaction if they exist.
      if (empty($original_rules) || !isset($original_rules[$rule_uuid])) {
        $condition_settings = $rule['condition']['settings'] ?? [];
        $reaction_settings = $rule['reaction']['settings'] ?? [];
        if ($condition = $this->_conditionNew($rule['condition']['id'], $condition_settings)) {
          $this->_conditionEvent($condition, 'onCreate');
        }
        if ($reaction = $this->_reactionNew($rule['reaction']['id'], $reaction_settings)) {
          $this->_reactionEvent($reaction, 'onCreate');
        }
      }
      // There are two cases here. If the plugin is the same, then we can trigger an onChange. But if the
      // plugins are different between original and new we'll trigger a delete on the original and an onCreate
      // the new.
      elseif (isset($original_rules[$rule_uuid])) {
        // First we have to create the old condition and reactions (will be '_none' if there is no
        // plugin specified).
        $original_rule = $original_rules[$rule_uuid];

        // Create the original condition and reactions and assign locally.
        $original_plugins = $this->_conditionReactionPlugins($original_rule);
        list ($original_condition, $original_reaction) = array_values($original_plugins);

        // Create objects for accessing the new condition and reaction.
        $condition_settings = $rule['condition']['settings'] ?? [];
        $reaction_settings = $rule['reaction']['settings'] ?? [];
        $new_condition = $this->_conditionNew($rule['condition']['id'], $condition_settings);
        $new_reaction = $this->_reactionNew($rule['reaction']['id'], $reaction_settings);

        // If the condition plugins are the same, trigger onChange. Otherwise, onDelete for the old and
        // onCreate for the new.
        if ($original_rule['condition']['id'] === $rule['condition']['id']) {
          $this->_conditionEvent($new_condition, 'onChange', $original_condition);
        }
        else {
          // Signal deletion of the original condition if not empty.
          if ($original_rule['condition']['id'] !== '_none') {
            $this->_conditionEvent($original_condition, 'onDelete');
          }
          // Signal creation of the new condition if not empty.
          if ($rule['condition']['id'] !== '_none') {
            $this->_conditionEvent($new_condition, 'onCreate');
          }
        }

        // If the reaction plugins are the same, trigger onChange. Otherwise, onDelete for the old and
        // onCreate for the new.
        if ($original_rule['reaction']['id'] === $rule['reaction']['id']) {
          $this->_reactionEvent($new_reaction, 'onChange', $original_reaction);
        }
        else {
          // Signal deletion of the original reaction if not empty.
          if ($original_rule['reaction']['id'] !== '_none') {
            $this->_reactionEvent($original_reaction, 'onDelete');
          }
          // Signal creation of the new reaction if not empty.
          if ($rule['reaction']['id'] !== '_none') {
            $this->_reactionEvent($new_reaction, 'onCreate');
          }
        }
      }
    }
  }

  /**
   * Create condition and reaction plugins based on the array (stored) version of a rule.
   *
   * @param array $rule
   *   The id (plugin id) of the condition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @return array
   *   An associative array of objects for the rule's Condition and Reaction.
   */
  protected function _conditionReactionPlugins(array $rule) {
    $plugins = [
      'condition' => NULL,
      'reaction' => NULL,
    ];

    // Ignore the case when the condition plugin ID is not set.
    if (isset($rule['condition'])
        && $this->conditionManager->hasDefinition($rule['condition']['id'])
    ) {
      $condition_settings = $rule['condition']['settings'] ?? [];
      $plugins['condition'] = $this->_conditionNew($rule['condition']['id'], $condition_settings);
    }
    // Ignore the case when the condition plugin ID is not set.
    if (isset($rule['reaction'])
        && $this->reactionManager->hasDefinition($rule['reaction']['id'])
    ) {
      $reaction_settings = $rule['reaction']['settings'] ?? [];
      $plugins['reaction'] = $this->_reactionNew($rule['reaction']['id'], $reaction_settings);
    }

    return $plugins;
  }

  /**
   * Helper method to create a condition entity from an array of ID and settings.
   *
   * @param string $id
   *   The id (plugin id) of the condition.
   * @param array $settings
   *   The settings of the condition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @return object|null
   *   The Condition created.
   */
  protected function _conditionNew($id, array $settings) {
    // Ignore the case when the condition plugin ID is not set.
    if ($id === '_none') {
      return;
    }

    // Extract the conditions settings, create a condition entity.
    $condition_settings = $settings ?? [];
    return $this->conditionManager->createInstance($id, $condition_settings);
  }

  /**
   * Helper method to create a reaction entity from an array of ID and settings.
   *
   * @param string $id
   *   The id (plugin id) of the condition.
   * @param array $settings
   *   The settings of the condition.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @return object|null
   *   The Reaction created.
   */
  protected function _reactionNew($id, array $settings) {
    // Ignore the case when the reaction plugin ID is not set.
    if ($id === '_none') {
      return;
    }

    // Extract the conditions settings, create a condition entity.
    $reaction_settings = $settings ?? [];
    return $this->reactionManager->createInstance($id, $reaction_settings);
  }

  /**
   * Helper method to invoke an event handler on a condition.
   *
   * @param Plugin\f1_p13n\Condition $condition
   *   The condition.
   * @param string $event
   *   The event (method) to call if it exists on the condition plugin.
   * @param object $context
   *   Context to pass to the event (such as a new version of the condition).
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function _conditionEvent($condition, $event, $context = NULL) {
    if (method_exists($condition, $event)) {
      $condition->{$event}($context);
    }
  }

  /**
   * Helper method to invoke an event handler on a reaction.
   *
   * @param Plugin\f1_p13n\Reaction\ $reaction
   *   The reaction.
   * @param string $event
   *   The event (method) to call if it exists on the reaction plugin.
   * @param array $context
   *   Context to pass to the event (such as a new version of the reaction).
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function _reactionEvent($reaction, $event, $context = NULL) {
    if (method_exists($reaction, $event)) {
      $reaction->{$event}($context);
    }
  }

}
