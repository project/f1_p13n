<?php

namespace Drupal\f1_p13n;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\f1_p13n\Annotation\Condition;
use Drupal\f1_p13n\Annotation\Reaction;
use Drupal\f1_p13n\Condition\ConditionInterface;
use Drupal\f1_p13n\Reaction\ReactionInterface;

/**
 * Provides a plugin manager for experience plugins.
 */
class ExperiencePluginManager extends DefaultPluginManager implements CategorizingPluginManagerInterface {

  use CategorizingPluginManagerTrait;

  /**
   * Constructor for experience plugin manager.
   *
   * @param string $type
   *   The plugin type.
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $type_annotations = [
      'Condition' => Condition::class,
      'Reaction' => Reaction::class,
    ];
    $plugin_interfaces = [
      'Condition' => ConditionInterface::class,
      'Reaction' => ReactionInterface::class,
    ];
    $type_id = strtolower($type);

    parent::__construct("Plugin/f1_p13n/$type", $namespaces, $module_handler, $plugin_interfaces[$type], $type_annotations[$type]);
    $this->alterInfo('f1_p13n_' . $type_id . '_info');
    $this->setCacheBackend($cache_backend, 'f1_p13n_' . $type_id . 'plugins');
  }

}
