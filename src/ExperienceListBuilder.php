<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Experience entities.
 *
 * @ingroup f1_p13n
 */
class ExperienceListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Experience ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\f1_p13n\Entity\Experience $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.f1_p13n_experience.edit_form',
      ['f1_p13n_experience' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
