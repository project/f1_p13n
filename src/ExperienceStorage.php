<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;

/**
 * Defines the storage handler class for experiences.
 *
 * This extends the base storage class, adding special handling methods
 * for experiences.
 */
class ExperienceStorage extends SqlContentEntityStorage implements ExperienceStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function loadByUuid($uuid) {
    $result = $this->loadByProperties(['uuid' => $uuid]);

    if ($result) {
      return reset($result);
    }
    else {
      return NULL;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function loadWithKey($key_field = NULL) {
    $experiences = $this->loadMultiple();

    if ($key_field) {
      // We'll try to recreate the array using the key requested, though we'll
      // ignore any experiences that do have a value for the key and later test
      // to make sure the two arrays have the same count.
      $keyed_experiences = [];
      foreach ($experiences as $experience) {
        if ($key_value = $experience->get($key_field)->getString()) {
          $keyed_experiences[$key_value] = $experience;
        }
        else {
          break;
        }
      }

      if (count($keyed_experiences) == count($experiences)) {
        $experiences = $keyed_experiences;
      }
    }

    return $experiences;
  }

}
