<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Condition;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\f1_p13n\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines a section dimension condition.
 *
 * @Condition(
 *   id = "section_condition",
 *   label = @Translation("Menu section"),
 *   category = @Translation("General"),
 *   dimension = "page_section"
 * )
 */
class SectionCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'dimension' => 'page_section',
      'any_value' => 0,
      'dimension_value' => ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'condition';

    $form['dimension'] = [
      '#type' => 'hidden',
      '#value' => 'page_section',
      '#default_value' => $this->configuration['dimension'],
    ];

    $form['dimension_value'] = [
      '#type' => 'textfield',
      '#title' => 'Website section',
      '#size' => '36',
      '#default_value' => $this->configuration['dimension_value'],
      '#placeholder' => 'Website section (e.g., "Visit")',
    ];

    $form['any_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Any value'),
      '#default_value' => $this->configuration['any_value'],
      '#description' => $this->t('Evaluates to true if any value in this category has been discovered.<br />Overrides the Website section setting.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
