<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Condition;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\f1_p13n\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines a content type dimension condition.
 *
 * @Condition(
 *   id = "content_type_condition",
 *   label = @Translation("Page type"),
 *   category = @Translation("General"),
 *   dimension = "page_type"
 * )
 */
class ContentTypeCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'dimension' => 'page_type',
      'any_value' => 0,
      'dimension_value' => ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'condition';

    $form['dimension'] = [
      '#type' => 'hidden',
      '#value' => 'page_type',
      '#default_value' => $this->configuration['dimension'],
    ];

    $form['dimension_value'] = [
      '#type' => 'textfield',
      '#size' => '36',
      '#title' => $this->t('Page type'),
      '#default_value' => $this->configuration['dimension_value'],
      '#placeholder' => 'Page type ("Blog post")',
    ];

    $form['any_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Any value'),
      '#default_value' => $this->configuration['any_value'],
      '#description' => $this->t('Evaluates to true if any value in this category has been discovered.<br />Overrides the Page type field.'),
    ];

    $this->getConfiguration();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
