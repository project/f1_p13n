<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Condition;

use Drupal\Core\Form\FormStateInterface;
use Drupal\f1_p13n\Condition\ConditionPluginBase;

/**
 * Defines a true condition.
 *
 * @Condition(
 *   id = "is_true",
 *   label = @Translation("Always true"),
 *   category = @Translation("Default/Override")
 * )
 */
class IsTrue extends ConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'condition';
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate($rule, $experience_dimensions) {
    return TRUE;
  }

}
