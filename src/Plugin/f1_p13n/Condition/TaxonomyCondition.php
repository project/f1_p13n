<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Condition;

use Drupal\f1_p13n\TokenizationService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\f1_p13n\Condition\ConditionPluginBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines a taxonomy dimension condition.
 *
 * @Condition(
 *   id = "taxonomy_condition",
 *   label = @Translation("Category"),
 *   category = @Translation("General"),
 *   dimension = "taxonomy"
 * )
 */
class TaxonomyCondition extends ConditionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The related content service.
   *
   * @var \Drupal\f1_p13n\TokenizationService
   */
  protected $tokenizationService;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\f1_p13n\Services\TokenizationService $tokenization_service
   *   The Related Content service.
   *
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    TokenizationService $tokenization_service
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->tokenizationService = $tokenization_service;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('f1_p13n.tokenization_service'),
    );
  }

  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'dimension' => '',
      'any_value' => 0,
      'dimension_value' => ''
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'condition';

    // Get the list of dimensions we are currently tracking
    $dimensions = $this->tokenizationService->getDiscoveryDimensionsVocabularies();

    $form['dimension'] = [
      '#type' => 'select',
      '#title' => 'Category',
      '#options' => $dimensions,
      '#empty_option' => $this->t('- None -'),
      '#description' => $this->t('Select a category to match, such as <em>Topics</em>.'),
    ];

    $default_dimension = $this->configuration['dimension'];
    if ($default_dimension && in_array($default_dimension, array_keys($dimensions))) {
      $form['dimension']['#default_value'] = $default_dimension;
    }

    $form['dimension_value'] = [
      '#type' => 'textfield',
      '#title' => 'Category term',
      '#size' => '36',
      '#default_value' => $this->configuration['dimension_value'],
      '#placeholder' => 'Category term (e.g., "Social Justice")',
      '#description' => $this->t('Specify a term to match, such as <em>Music</em>.'),
    ];

    $form['any_value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Any value'),
      '#default_value' => $this->configuration['any_value'],
      '#description' => $this->t('Evaluates to true if any value in this category has been discovered.<br />Overrides the Category term field.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

}
