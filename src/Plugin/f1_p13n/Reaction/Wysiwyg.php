<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Reaction;

use Drupal\Core\Form\FormStateInterface;
use Drupal\f1_p13n\Reaction\ReactionPluginBase;

/**
 * Defines a reaction rendering a WYSIWYG field.
 *
 * @Reaction(
 *   id = "wysiwyg",
 *   label = @Translation("Formatted text"),
 *   category = @Translation("Text"),
 * )
 */
class Wysiwyg extends ReactionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'wysiwyg' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'reaction';
    $form['wysiwyg'] = [
      '#type' => 'text_format',
      '#title' => t('Formatted text'),
      '#default_value' => $this->configuration['wysiwyg']['value'] ?? NULL,
      '#description' => $this->t('Enter the formatted text to display for personalization experience. Image and document embedding is permitted; video embedding should be avoided.'),
      '#format' => 'full_html',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function view($dimensions = []) {
    return [
      '#theme' => 'p13n_reaction',
      '#plugin' => 'wysiwyg',
      '#tag' => 'div',
      '#content' => [
        '#type' => 'processed_text',
        '#text' => $this->configuration['wysiwyg']['value'],
        '#format' => 'full_html',
      ],
    ];
  }

}
