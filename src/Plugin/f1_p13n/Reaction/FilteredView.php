<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Reaction;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\f1_p13n\Reaction\ReactionPluginBase;
use Drupal\f1_p13n\TokenizationService;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a reaction rendering a view with contextual filter conditions.
 *
 * @Reaction(
 *   id = "filtered_view",
 *   label = @Translation("Filtered view"),
 *   category = @Translation("Content list"),
 *   dimension = "taxonomy"
 * )
 */
class FilteredView extends ReactionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The related content service.
   *
   * @var TokenizationService
   */
  protected $tokenizationService;

  /**
   * The renderer.
   *
   * @var RendererInterface
   */
  protected $renderer;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\f1_p13n\Services\TokenizationService $tokenization_service
   *   The Related Content service.
   * @param RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TokenizationService $tokenization_service, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->tokenizationService = $tokenization_service;
    $this->renderer = $renderer;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('f1_p13n.tokenization_service'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'view' => '',
      'view_args' => '',
      'dimensions' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = [];

    // Get all views displays and create the options list for the select.
    $options = [];
    $views = Views::getAllViews();

    foreach ($views as $view_id => $view) {
      foreach ($view->get('display') as $display_id => $display) {
        // Do not populate the list with page displays, they cannot be used for fulfillment.
        if ($display['display_plugin'] === 'page') {
          continue;
        }
        // The option ID is view_id.display_id but if the display is the default display then do not display it
        // in the option label.
        $options[$view_id . '.' . $display_id] = $this->t('@view@display', [
          '@view' => $view->label(),
          '@display' => $display['id'] == 'default' ? '' : $this->t(' -- @display_title ', [
            '@display_title' => $display['display_title'],
          ]),
        ]);
      }
    }

    $form['view'] = [
      '#type' => 'select',
      '#title' => $this->t('View'),
      '#title_display' => 'before',
      '#default_value' => $this->configuration['view'],
      '#options' => $options,
      '#empty_option' => $this->t('- Select -'),
      '#description' => $this->t('View to use to display personalized content to the user.'),
    ];

    // Class the form.
    $form['#attributes']['class'][] = 'reaction';

    $form['view_args'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Arguments'),
      '#default_value' => $this->configuration['view_args'],
      '#size' => 64,
      '#description' => $this->t('Specify text values to filter the results of the view. Values are case-sensitive and whitespace is permitted within a value. Separate values within the same position using a "+" sign. Separate positions using a comma (","). Use the value "all" to indicate no filtering at a position. Tokens may be used and will be replaced with dimension values.'),
    ];

    // Get the list of taxonomy dimensions we are currently tracking
    $dimensions = $this->tokenizationService->getDiscoveryDimensions();

    // Create the display list.
    $dimension_list = [];
    foreach ($dimensions as $key => $label) {
      $dimension_list[] = $this->t("@label: [@key]", [ "@label" => $label, "@key" => $key ]);
    }

    // Create a render array unordered list of the tokens.
    $token_list = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $dimension_list,
      '#wrapper_attributes' => ['class' => 'container'],
    ];

    // Include the token list in the form with instructions.
    $form['tokens'] = [
      '#type' => 'details',
      '#title' => $this->t('Available tokens'),
      '#open' => FALSE,
    ];
    $form['tokens']['token_list'] = [
      '#type' => 'item',
      '#markup' => $this->renderer->renderPlain($token_list),
      '#description' => <<<'EOD'
Insert the token into the Reaction text field using square brackets, for example "[topics]".<br />
The dimension value or values will be inserted where the token is placed.<br />
Multiple dimension values as a result of "ties" will be used as a "+" separated filter list, such as "Music+Social Justice."
EOD,
    ];

    $form['dimensions'] = [
      '#type' => 'hidden',
      '#default_value' => $this->configuration['dimensions'],
      '#element_validate' => [
        [get_class($this), 'dimensionElementValidation'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function view($dimensions = []) {
    $build = [];

    // Retrieve the view ID and display.
    list ($view_id, $view_display) = explode('.', $this->configuration['view']);

    // Need to have a view_id and display or we have nothing to display.
    if (!$view_id || !$view_display) {
      return $build;
    }

    // Prepare the term string for use as a set of arguments with default separtors.
    $filter_array = $this->tokenizationService->explodeFilterList($this->configuration['view_args']);

    // Resolve any tokens that were specified as filter values.
    $filter_array = $this->tokenizationService->resolveFilterListTokens($filter_array, $dimensions);

    // Create the argument list using insertion of all, etc.
    $arglist = $this->tokenizationService->createFilterList($filter_array, ['dash_replace' => FALSE]);

    // Remove duplicates.
    $arglist = $this->tokenizationService->arglistRemoveDuplicates($arglist);

    // Get the view.
    $view = Views::getView($view_id);

    // It's possible there's no access allowed, in which case return nothing.
    if ($view && $view->access($view_display)) {
      $view->setDisplay($view_display);

      // Set the arguments to ensure correct contextual filtering.
      $view->setArguments($arglist);
      $view->execute();

      $build = [
        '#theme' => 'p13n_reaction',
        '#plugin' => 'filtered_view',
        '#tag' => 'div',
        '#content' => [
          '#type' => 'view',
          '#name' => $view_id,
          '#display_id' => $view_display,
          '#arguments' => $arglist,
          '#view' => $view,
        ],
      ];
    }

    return $build;
  }

  /**
   * A callback for validating the filteredView form.
   *
   * @param array $element
   *   The form element.
   * @param FormStateInterface $form_state
   *   The form state for the entire form.
   * @param array $form
   *   The entire form.
   */
  public static function dimensionElementValidation(array &$element, FormStateInterface $form_state, array &$form) {
    // Extract the names of any dimensions passed in the view_args as tokens
    // formatted like [name] and store them as a comma separated list in the
    // hidden dimensions field on the reaction configuration form.
    $view_args_parents = array_slice($element['#parents'], 0, -1);
    $view_args_parents[] = 'view_args';
    $view_args = $form_state->getValue($view_args_parents);

    // Extract the dimensional tokens from the argument string. This produces an array of dimensions.
    $dimensions = TokenizationService::extractArglistDimensionTokens($view_args);

    // If dimension tokens were identified, set the list of target dimensions as a comma separated list
    // into the hidden form field.
    if (!empty($dimensions)) {
      $dimensions_string = implode(',', $dimensions);
      $form_state->setValue($element['#parents'], $dimensions_string);
    }
  }

}
