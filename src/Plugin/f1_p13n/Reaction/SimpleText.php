<?php

namespace Drupal\f1_p13n\Plugin\f1_p13n\Reaction;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\f1_p13n\Reaction\ReactionPluginBase;
use Drupal\f1_p13n\TokenizationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a reaction rendering a text field.
 *
 * @Reaction(
 *   id = "simple_text",
 *   label = @Translation("Simple text"),
 *   category = @Translation("Text")
 * )
 */
class SimpleText extends ReactionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The related content service.
   *
   * @var \Drupal\f1_p13n\TokenizationService
   */
  protected $tokenizationService;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\f1_p13n\Services\TokenizationService $tokenization_service
   *   The Related Content service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   *
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, TokenizationService $tokenization_service, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->tokenizationService = $tokenization_service;
    $this->renderer = $renderer;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('f1_p13n.tokenization_service'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'reaction_text' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['#attributes']['class'][] = 'reaction';

    $form['reaction_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reaction text'),
      '#default_value' => $this->configuration['reaction_text'],
      '#description' => t('Specify the text to display. The text can include the tokens listed below.'),
      '#size' => 60,
    ];

    $form['dimensions'] = [
      '#type' => 'hidden',
      '#default_value' => $this->configuration['dimensions'] ?? NULL,
      '#element_validate' => [
        [get_class($this), 'dimensionElementValidation'],
      ],
    ];

    // Get the list of taxonomy dimensions we are currently tracking.
    $dimensions = $this->tokenizationService->getDiscoveryDimensions();

    // Create the display list.
    $dimension_list = [];
    foreach ($dimensions as $key => $label) {
      $dimension_list[] = $this->t("@label: [@key]", [ "@label" => $label, "@key" => $key ]);
    }

    // Create a render array unordered list of the tokens.
    $token_list = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $dimension_list,
      '#wrapper_attributes' => ['class' => 'container'],
    ];

    // Include the token list in the form with instructions.
    $form['tokens'] = [
      '#type' => 'details',
      '#title' => $this->t('Available tokens'),
      '#open' => FALSE,
    ];
    $form['tokens']['token_list'] = [
      '#type' => 'item',
      '#markup' => $this->renderer->renderPlain($token_list),
      '#description' => <<<'EOD'
Insert the token into the Reaction text field using square brackets, for example "[topics]".<br />
The dimension value or values will be inserted where the token is placed.<br />
Multiple dimension values as a result of "ties" will be inserted into the string as a comma separated list, such as "Music, Social Justice"

EOD,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function view($dimensions = []) {
    $build = [];

    // Retrieve the reaction text and return nothing renderable if empty.
    $reaction_text = $this->configuration['reaction_text'];
    if (!$reaction_text) {
      return $build;
    }

    // @todo Retrieve this from form storage or configuration as set in dimensionElementValidation so
    // the extract doesn't have to be done twice. Need to have the list of tokens in the source reaction
    // string so we can delete them if no dimensional value was returned.
    $tokens = TokenizationService::extractTextDimensionTokens($reaction_text);
    if ($tokens) {
      $reaction_text = TokenizationService::replaceTextTokens($reaction_text, $tokens, $dimensions);
    }

    // Build the reaction text.
    $build = [
      '#theme' => 'p13n_reaction',
      '#plugin' => 'simple_text',
      '#tag' => 'p',
      '#content' => [
        '#type' => '#markup',
        '#markup' => $reaction_text,
      ],
    ];

    return $build;
  }

  /**
   * A callback for validating the filteredView form.
   *
   * @param array $element
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state for the entire form.
   * @param array $form
   *   The entire form.
   */
  public static function dimensionElementValidation(array &$element, FormStateInterface $form_state, array &$form) {
    // Extract the names of any dimensions passed in the view_args as tokens
    // formatted like [name] and store them as a comma separated list in the
    // hidden dimensions field on the reaction configuration form.
    $view_args_parents = array_slice($element['#parents'], 0, -1);
    $view_args_parents[] = 'reaction_text';
    $reaction_text = $form_state->getValue($view_args_parents);

    // Extract the dimensional tokens from the argument string. This produces an array of dimensions.
    $dimensions = TokenizationService::extractTextDimensionTokens($reaction_text);

    // If dimension tokens were identified, set the list of target dimensions as a comma separated list
    // into the hidden form field.
    if (!empty($dimensions)) {
      $dimensions_string = implode(',', $dimensions);
      $form_state->setValue($element['#parents'], $dimensions_string);
    }
  }

}
