<?php

namespace Drupal\f1_p13n\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines the 'f1_p13n_experience_reference' entity field type.
 *
 * @FieldType(
 *   id = "f1_p13n_experience_reference",
 *   label = @Translation("Experience"),
 *   description = @Translation("A reference to a Personalization experience."),
 *   category = @Translation("Personalization"),
 *   default_widget = "f1_p13n_experience_reference_autocomplete",
 *   default_formatter = "f1_p13n_experience_default",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class ExperienceReferenceItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'target_type' => 'f1_p13n_experience',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $element = parent::storageSettingsForm($form, $form_state, $has_data);
    $element['target_type']['#access'] = FALSE;
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
