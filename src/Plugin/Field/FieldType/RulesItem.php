<?php

namespace Drupal\f1_p13n\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\MapItem;

/**
 * Defines the 'f1_p13n_rules' entity field type.
 *
 * @FieldType(
 *   id = "f1_p13n_rules",
 *   label = @Translation("Rules"),
 *   description = @Translation("An entity field for storing rules as a serialized array of values."),
 *   no_ui = TRUE,
 *   default_formatter = "f1_p13n_rules_default",
 *   list_class = "\Drupal\Core\Field\MapFieldItemList",
 * )
 *
 *  @todo: Maybe combine getX into one function?
 *
 */
class RulesItem extends MapItem {

  /**
   * Get the conditions used by a set of rules.
   */
  public function getConditions() {
    $rules = $this->value;
    $conditions = [];

    foreach ($rules as $rule) {
      array_push($conditions, $rule['condition']);
    }

    return $conditions;
  }

  /**
   * Get the reactions used by a set of rules.
   */
  public function getReactions() {
    $rules = $this->value;
    $reactions = [];

    foreach ($rules as $rule) {
      array_push($reactions, $rule['reaction']);
    }

    return $reactions;
  }


  /**
   * Get the dimensions used by this rule's conditions and reactions.
   */
  public function getDimensions() {
    $rules = $this->value;
    $dimensions = [];

    foreach ($rules as $rule) {
      // Get condition dimensions.
      if (!empty($rule['condition']['settings']['dimension'])) {
        $condition_dimension = $rule['condition']['settings']['dimension'];
        array_push($dimensions, $condition_dimension);
      }

      // Get reaction dimensions.
      if (!empty($rule['reaction']['settings']['dimensions'])) {
        $reaction_dimensions = explode(',', $rule['reaction']['settings']['dimensions']);
        foreach ($reaction_dimensions as $dimension) {
          array_push($dimensions, $dimension);
        }
      }
    }

    return array_unique($dimensions);
  }

}
