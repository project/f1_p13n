<?php

namespace Drupal\f1_p13n\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteWidget;

/**
 * Autocomplete widget for 'f1_p13n_experience_reference'.
 *
 * @FieldWidget(
 *   id = "f1_p13n_experience_reference_autocomplete",
 *   label = @Translation("Autocomplete"),
 *   description = @Translation("An autocomplete text field."),
 *   field_types = {
 *     "f1_p13n_experience_reference"
 *   }
 * )
 */
class ExperienceReferenceAutocompleteWidget extends EntityReferenceAutocompleteWidget {

}
