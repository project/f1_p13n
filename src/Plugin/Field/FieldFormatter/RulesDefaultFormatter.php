<?php

namespace Drupal\f1_p13n\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'f1_p13n_rules_default'.
 *
 * @FieldFormatter(
 *   id = "f1_p13n_rules_default",
 *   module = "f1_p13n",
 *   label = @Translation("Rules Default"),
 *   field_types = {
 *     "f1_p13n_rules"
 *   }
 * )
 *
 * @todo Simplify call to plugin managers.
 */
class RulesDefaultFormatter extends FormatterBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $conditionManager;

  /**
   * The reaction plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $reactionManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->conditionManager = $container->get('plugin.manager.f1_p13n.condition');
    $instance->reactionManager = $container->get('plugin.manager.f1_p13n.reaction');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'experience_dimensions' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $value = $item->value;
      foreach ($value as $rule) {
        if (!empty($rule['condition']['id'])
            && $this->conditionManager->hasDefinition($rule['condition']['id'])
        ) {
          // Get configured condition.
          $condition_settings = $rule['condition']['settings'] ?? [];
          $condition = $this->conditionManager->createInstance($rule['condition']['id'], $condition_settings);

          // If condition is true and reaction plugin exists or reaction is none
          // provide reaction and break. If condition is not true or reaction 
          // plugin does not exist continue evaulating rules.
          $experience_dimensions = $this->getSetting('experience_dimensions');
          if ($condition->evaluate($rule, $experience_dimensions)
              && ($this->reactionManager->hasDefinition($rule['reaction']['id'])
                  || $rule['reaction']['id'] == '_none'
              )
          ) {
            if ($rule['reaction']['id'] != '_none') {
              $dimension_and_values = [];
              // Get configured reaction.
              $reaction_settings = $rule['reaction']['settings'] ?? [];
              if (!empty($rule['condition']['settings']['dimension'])) {
                // Get dimension from rule.
                $dimension = $rule['condition']['settings']['dimension'];
                // Get value from experience dimensions object.
                $dimension_value = $experience_dimensions[$dimension];
                // We only ever return one value. Pass dimension
                // and value to view.
                $dimension_and_values[$dimension] = $dimension_value;
              }
              if (!empty($rule['reaction']['settings']['dimensions'])) {
                $reaction_dimensions = explode(",", $rule['reaction']['settings']['dimensions']);
                foreach ($reaction_dimensions as $dimension) {
                  // Get value from experience dimensions object.
                  $dimension_value = $experience_dimensions[$dimension];
                  // We only ever return one value. Pass dimension and
                  // value to view.
                  $dimension_and_values[$dimension] = $dimension_value;
                }
              }
              $reaction = $this->reactionManager->createInstance($rule['reaction']['id'], $reaction_settings);
              $elements[$delta] = $reaction->view($dimension_and_values);
            }
            break;
          }
        }
      }
    }
    return $elements;
  }

}
