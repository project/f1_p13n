<?php

namespace Drupal\f1_p13n\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'experience_default' formatter.
 *
 * @FieldFormatter(
 *   id = "f1_p13n_experience_default",
 *   module = "f1_p13n",
 *   label = @Translation("Experience Default"),
 *   field_types = {
 *     "f1_p13n_experience_reference"
 *   }
 * )
 *
 * @todo Simplify call to plugin managers.
 */
class ExperienceDefaultFormatter extends FormatterBase {

  /**
   * The condition plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $conditionManager;

  /**
   * The reaction plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $reactionManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );

    $instance->conditionManager = $container->get('plugin.manager.f1_p13n.condition');
    $instance->reactionManager = $container->get('plugin.manager.f1_p13n.reaction');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'theme' => 'p13n_experience_block',
      'pid' => '',
      'dimensions' => '',
      'html_tag' => 'div',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $has_active_campaign = $item->entity->hasActiveCampaign();
      if ($has_active_campaign) {
        $pid = $item->entity->uuid->value;
        $rules_item = $item->entity->rules[0] ?? [];
        $dimension_list = !empty($rules_item) ? $rules_item->getDimensions() : [];
        $output = [
          '#theme' => 'p13n_experience_block',
          '#pid' => base64_encode($pid),
          '#dimensions' => array_unique($dimension_list),
          '#tag' => 'div',
        ];
        $elements['#attached']['library'][] = 'f1_p13n/fulfillment';
        $elements[$delta] = $output;
      }
    }

    return $elements;
  }

}
