<?php

namespace Drupal\f1_p13n\Plugin\metatag\Group;

use Drupal\metatag\Plugin\metatag\Group\GroupBase;

/**
 * The advanced group.
 *
 * @MetatagGroup(
 *   id = "p13n",
 *   label = @Translation("Personalization"),
 *   description = @Translation("Controls how information is used for content personalization."),
 *   weight = 3
 * )
 */
class Personalization extends GroupBase {
  // Inherits everything from Base.
}
