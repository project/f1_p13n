<?php

namespace Drupal\f1_p13n\Plugin\metatag\Tag;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuActiveTrailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\metatag\Plugin\metatag\Tag\MetaPropertyBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\f1_p13n\RdfaService;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Dimension metatag tag.
 *
 * @MetatagTag(
 *   id = "p13n_dimension",
 *   label = @Translation("Tracking dimensions"),
 *   description = @Translation("Specifies which dimensions of the page are used to build the user model."),
 *   name = "pn:",
 *   group = "p13n",
 *   weight = 1,
 *   type = "label",
 *   secure = FALSE,
 *   multiple = TRUE
 * )
 */
class Dimension extends MetaPropertyBase implements ContainerFactoryPluginInterface {

  /**
   * The menu active trail service.
   *
   * @var \Drupal\Core\Menu\MenuActiveTrail
   */
  protected $menuActiveTrail;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The RDFa service.
   *
   * @var \Drupal\f1_p13n\RdfaService
   */
  protected $rdfaService;

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * Constructs a new eventsBlock instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\f1_p13n\RdfaService $rdfa_service
   *   The RDFa service.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The routeMatch service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Menu\MenuActiveTrailInterface $menu_active_trail
   *   The menu active trail service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RdfaService $rdfa_service,
    RouteMatchInterface $route_match,
    EntityTypeManagerInterface $entity_type_manager,
    MenuActiveTrailInterface $menu_active_trail,
    EntityRepositoryInterface $entity_repository
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->rdfaService = $rdfa_service;
    $this->routeMatch = $route_match;
    $this->entityTypeManager = $entity_type_manager;
    $this->menuActiveTrail = $menu_active_trail;
    $this->entityRepository = $entity_repository;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('f1_p13n.rdfa_service'),
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('menu.active_trail'),
      $container->get('entity.repository')
    );
  }

  /**
   * Sets the value of this tag.
   *
   * @param string|array $value
   *   The value to set to this tag.
   *   It can be an array if it comes from a form submission or from field
   *   defaults, in which case
   *   we transform it to a comma-separated string.
   */
  public function setValue($value) {
    if (is_array($value)) {
      $value = array_filter($value);
      $value = implode(',', array_keys($value));
    }
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   *
   * @todo Adjust description based on location of topic settings.
   * Should the key or the label reflect internal nomenclature,
   * i.e Categories = Topics or taxonomy = topics?
   */
  public function form(array $element = []) {
    // Prepare the default value as it is stored as a string.
    $default_value = [];

    if (!empty($this->value)) {
      $default_value = explode(',', $this->value);
    }

    $form = [
      '#type' => 'checkboxes',
      '#title' => $this->label(),
      '#description' => $this->description(),
      '#options' => [
        'taxonomy' => $this->t('Taxonomy categories (defined in Personalization module settings)'),
        'section' => $this->t('Section (the top element of the page\'s associated primary menu item if available)'),
        'bundle' => $this->t('Content type (automatically generated from content type, such as <em>Article</em> or <em>Landing page</em> )'),
      ],
      '#default_value' => $default_value,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function output() {
    $elements = [];

    // Value will be an array that can include taxonomy, section, bundle.
    $dimensions = explode(",", $this->value());

    // If there are no requests for this node or class of node, ignore.
    if (empty($dimensions)) {
      return $elements;
    }

    // Retrieve the context entity. Only check for term, node (or revision/preview), or media entity.
    $context_entity = NULL;

    if (($route_object = $this->routeMatch->getRouteObject())) {
      $route_contexts = $route_object->getOption('parameters');
      // Check for a node revision parameter first.
      if (isset($route_contexts['node_revision']) && $revision = $this->routeMatch->getParameter('node_revision')) {
        $context_entity = $revision;
      }
      elseif (isset($route_contexts['node']) && $node = $this->routeMatch->getParameter('node')) {
        $context_entity = $node;
      }
      elseif (isset($route_contexts['node_preview']) && $node = $this->routeMatch->getParameter('node_preview')) {
        $context_entity = $node;
      }
      elseif (isset($route_contexts['taxonomy_term']) && $term = $this->routeMatch->getParameter('taxonomy_term')) {
        $context_entity = $term;
      }
      elseif (isset($route_contexts['media']) && $media_item = $this->routeMatch->getParameter('media')) {
        $context_entity = $media_item;
      }
    }
    
    // Should be unlikely but special pages may exist.
    if ($context_entity == NULL) {
      return $elements;
    }

    // Output for taxonomy terms.
    if (in_array('taxonomy', $dimensions)) {
      // Retrieve the list of taxonomy dimensions (vocabulary machine IDs) to use for user modelling.
      $target_vids = $this->rdfaService->getTaxonomyDimensions();

      // Get the specific related terms.
      $taxonomy_dimensions = $this->rdfaService->getEntityRelatedTerms($target_vids, $context_entity);

      foreach ($taxonomy_dimensions as $dimension => $values) {
        foreach ($values as $dimension_value) {
          $elements[] = [
            '#tag' => 'meta',
            '#attributes' => [
              $this->nameAttribute => $this->name() . $dimension,
              'content' => $dimension_value,
            ],
          ];
        }
      }
    }

    // Output for bundle.
    if (in_array('bundle', $dimensions)) {
      // Retrieve the label for the bundle.
      $bundle_type_id = $context_entity->getEntityType()->getBundleEntityType();
      $bundle_label = $this->entityTypeManager
        ->getStorage($bundle_type_id)
        ->load($context_entity->bundle())
        ->label();

      // Write the meta tag.
      $elements[] = [
        '#tag' => 'meta',
        '#attributes' => [
          $this->nameAttribute => $this->name() . 'page_type',
          'content' => $bundle_label,
        ],
      ];
    }

    // Retrieve an array of content section labels. At the moment only the top level
    // menu item is returned.
    // @todo option in settings to expose all ancestor labels of menu hierarchy as separate tags.
    $labels = $this->rdfaService->getContentSectionLabels('main');
    foreach ($labels as $label) {
      // Create the section tag.
      $elements[] = [
        '#tag' => 'meta',
        '#attributes' => [
          $this->nameAttribute => $this->name() . 'page_section',
          'content' => $label,
        ],
      ];
    }

    return $elements;
  }

}
