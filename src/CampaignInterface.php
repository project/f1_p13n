<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Campaign entities.
 *
 * @ingroup f1_p13n
 */
interface CampaignInterface extends ContentEntityInterface {

}
