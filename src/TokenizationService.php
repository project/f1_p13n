<?php

namespace Drupal\f1_p13n;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;


/**
 * A class containing methods for "tokenization" services.
 */
class TokenizationService {
  use StringTranslationTrait;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $vocabStorage;

  /**
   * Dimensions settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Array of separators to use by default for filter string parsing.
   *
   * @var array
   */
  private static $defaultSeparators = ['term' => '+', 'position' => ','];

  /**
   * Array of separators to use by default for filter string parsing.
   *
   * @var string
   */
  private static $defaultParameter = 'all';

  /**
   * Constructs a new TokenizationService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration service interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory
  ) {

    $this->vocabStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');
    $this->configFactory = $config_factory;
  }

  /**
   * Explode a string into an argument array.
   *
   * Here we're going to break down the term list into its component
   * pieces. This provides a hook for processing of terms (such as lookup
   * against taxonomy terms, etc.) and also the string replacement
   * of whitespace into dashes.
   *
   * @param string $filter_string
   *   A string filter list to explode.
   * @param string[] $separators
   *   An array of separators for term and argument list.
   *
   * @return array
   *   Array of term IDs for the terms that signify an event is live.
   */
  public static function explodeFilterList(string $filter_string, $separators = []) {
    // Use either the default separators or argument.
    $separators = $separators ?: self::$defaultSeparators;

    // The result filter array.
    $filter_array = [];

    // First start by breaking the list into the arguments, which should
    // corresponding to the view (contextual filter) arguments.
    $value_list = explode($separators['position'], $filter_string);

    // For each of the component argument lists, explode each position into separate terms.
    foreach ($value_list as $ix => $values) {
      $filter_terms = explode($separators['term'], $values);

      // Trim the strings just in case a bit of whitespace snuck in.
      $filter_array["arg$ix"] = array_map('trim', $filter_terms);
    }

    return $filter_array;
  }

  /**
   * Create argument list of filter values.
   *
   * This function creates an argument list for views or other consumers
   * of the form [TERM]+[TERM] with spaces optionally replaced by
   * dashes and the empty list for a taxonomy replaced by 'all' for
   * views contextual filters.
   *
   * @param array $filter_array
   *   An array of target vocabulary IDs and associated terms.
   * @param array $options
   *   Options for creating the argument list.
   *
   * @return array
   *   Array of term IDs for the terms that signify an event is live.
   */
  public function createFilterList($filter_array, $options = []) {
    $default_options = [
      'separators' => $options['separators'] ?? self::$defaultSeparators,
      'dash_replace' => $options['dash_replace'] ?? TRUE,
      'empty_string' => $options['empty_string'] ?? self::$defaultParameter,
    ];

    $filter_values = [];

    // Return the filter targets for embedding in the drupal_view call.
    // Multiple targets are separated by a term separator in the separators array, default is a plus sign
    // to match what is used in views contextual filters.
    foreach ($filter_array as $vid => $value_array) {
      if (empty($value_array)) {
        $filter_values[$vid] = $default_options['empty_string'];
      }
      else {
        // Unless dash replace was specifically not requested, convert spaces to dashes.
        if ($default_options['dash_replace']) {
          array_walk($value_array, function (&$value, &$key) {
            $value = str_replace(' ', '-', $value);
          });
        }

        // Implode the value array into a string using the separator.
        $filter_values[$vid] = implode($default_options['separators']['term'], $value_array);
      }
    }

    return $filter_values;
  }

  /**
   * Resolves tokens in a text field (string)
   *
   * @param array $filter_array
   *   The filter array containing values that may contain tokens to resolve.
   * @param array $dimensions
   *   The dimensional value array.
   * @param array $separators
   *   Different separators for term and position if desired, otherwise use static property.
   * @param string $default_parameter
   *   Different default parameter, otherwise use static property.
   *
   * @return array
   *   Returns the resolved list of filter tokens.
   */
  public function resolveFilterListTokens($filter_array, $dimensions, $separators = [], $default_parameter = '') {
    // If there are no arguments, no work to be done. However, even if there are no dimensional values it is
    // important to resolve tokens. Tokens that refer to missing taxonomy values will be resolved to the default
    // parameter, such as 'all'.
    if (empty($filter_array)) {
      return $filter_array;
    }

    // Use either the default separators or argument.
    $separators = $separators ?: self::$defaultSeparators;
    $default_parameter = $default_parameter ?: self::$defaultParameter;

    // Array walk to resolve filter tokens.
    array_walk($filter_array, [$this, 'resolveFilterTokens'], [
      'dimensions' => $dimensions,
      'separators' => $separators,
      'default_parameter' => $default_parameter,
    ]);
    return $filter_array;
  }

  /**
   * Resolve a single filter token or array of tokens. Function for use in array_walk.
   *
   * @param mixed $arg_mixed
   *   The argument that may contain a token, either a string or an array of strings.
   * @param int $key
   *   Used for internal function invocation.
   * @param array $options
   *   An array of options for dimensions list, separators, etc..
   *
   * @return mixed
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  private function resolveFilterTokens(&$arg_mixed, $key, $options) {
    // Extract the options if coming from a limited signature such as array_walk.
    list($dimensions, $separators, $default_parameter) = array_values($options);

    if (is_string($arg_mixed)) {
      $arg_mixed = $this->resolveFilterTokenIndividual($arg, $dimensions, $separators, $default_parameter);
    }
    elseif (is_array($arg_mixed)) {
      foreach ($arg_mixed as &$arg) {
        $arg = $this->resolveFilterTokenIndividual($arg, $dimensions, $separators, $default_parameter);
      }
    }
    return $arg_mixed;
  }

  /**
   * Resolve a single filter token.
   *
   * @param string $arg
   *   The argument that may contain a token.
   * @param array $dimensions
   *   The list of token resolution values.
   * @param array $separators
   *   An array of separators (term, position)
   * @param string $default_parameter
   *   A string representing the default parameter to use (such as "all")
   *
   * @return string
   *   Returns the arg string with tokens resolved against dimension values.
   */
  protected function resolveFilterTokenIndividual(string &$arg, $dimensions, $separators, $default_parameter) {
    if (self::isToken($arg)) {
      // Extract the dimension name from the token.
      $dimension_name = self::extractToken($arg);

      // If there's a value for that dimension, use it. Otherwise use the default.
      if (isset($dimensions[$dimension_name]) && !empty($dimensions[$dimension_name])) {
        $arg = is_array($dimensions[$dimension_name])
          ? implode($separators['term'], array_values($dimensions[$dimension_name]))
          : $dimensions[$dimension_name];
      }
      else {
        $arg = $default_parameter;
      }
    }

    return $arg;
  }

  /**
   * Determines whether an argument is a dimension token.
   *
   * @param string $arg
   *   The argument that may contain a token.
   *
   * @return bool
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public static function isToken($arg) {
    return strpos($arg, '[') === 0;
  }

  /**
   * Extracts a token from a string that purports to contain one.
   *
   * @param string $arg
   *   The argument that may contain a token.
   *
   * @return string
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public static function extractToken($arg) {
    return str_replace(['[', ']'], '', $arg);
  }

  /**
   * Extracts a token from a string that purports to contain one.
   *
   * @param string $str
   *   The argument that may contain a token.
   *
   * @return string
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  protected static function formToken($str) {
    return "[$str]";
  }

  /**
   * Removes duplicate arguments.
   *
   * @param array $arglist
   *   The argument that may contain a token.
   *
   * @return array
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public function arglistRemoveDuplicates($arglist) {
    // Use either the default separators or argument.
    $separators = self::$defaultSeparators;

    foreach ($arglist as &$arg) {
      // Explode using term separator.
      $arg_array = explode($separators['term'], $arg);
      // Make unique, reimplode using term separator.
      $arg = implode($separators['term'], array_unique($arg_array));
    }

    return $arglist;
  }

  /**
   * Extract token dimensions from a string argument list.
   *
   * Argument lists can be separated by position, and each position can contain terms
   * separated by a different separator.
   *
   * @param string $arglist
   *   The argument that may contain a token, either a string or an array of strings.
   * @param array $separators
   *   An array of separators (term, position)
   *
   * @return array
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public static function extractArglistDimensionTokens($arglist, $separators = []) {
    $dimensions = [];

    // Explode the argument list, using the separators passed into this method. If empty
    // the class static separators will be used. String trimming is done by explodeFilterList.
    $filter_array = self::explodeFilterList($arglist, $separators);

    foreach ($filter_array as $arg_array) {
      foreach ($arg_array as $arg) {
        if (self::isToken($arg)) {
          $dimensions[] = self::extractToken($arg);
        }
      }
    }

    return $dimensions;
  }

  /**
   * Extract token dimensions from a string argument list.
   *
   * Argument lists can be separated by position, and each position can contain terms
   * separated by a different separator.
   *
   * @param string $text
   *   The argument that may contain a token, either a string or an array of strings.
   *
   * @return array
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public static function extractTextDimensionTokens($text) {
    $matches = [];
    $dimensions = [];

    // Find all possible tokens using pregmatch and extract the array of possible dimensions from
    // the returned matches.
    preg_match_all("/\[[^\]]*\]/", $text, $matches, PREG_PATTERN_ORDER);

    foreach ($matches[0] as $ix => $match) {
      // Confirm that what was matched is a valid personalization token and extract it into the correct form
      // for storage on the experience.
      if (!self::isToken($match)) {
        unset($dimensions[$ix]);
      }
      else {
        $dimensions[] = self::extractToken($match);
      }
    }

    return $dimensions;
  }

  /**
   * Extract token dimensions from a string argument list.
   *
   * Argument lists can be separated by position, and each position can contain terms
   * separated by a different separator.
   *
   * @param string $text
   *   The argument that may contain a token, either a string or an array of strings.
   * @param array $tokens
   *   The list of tokens that should be replaced if dimension value exists.
   * @param array $dimensions
   *   The list of dimensional values returned from the user model.
   *
   * @return string
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  public static function replaceTextTokens($text, $tokens, $dimensions) {
    // Loop through tokens, do a simple str_replace.
    foreach ($tokens as $token) {
      if (isset($dimensions[$token])) {
        // Create a display string. This uses an override separator to display as a comma separated list.
        // @todo support the Oxford comma.
        $dimension_string = self::dimensionValuesDisplayString($dimensions[$token], ['term' => ', ']);
        $text = str_replace(self::formToken($token), $dimension_string, $text);
      }
      else {
        $text = str_replace(self::formToken($token), '', $text);
      }
    }

    return $text;
  }

  /**
   * Creates an array of dimension values into a display string.
   *
   * @param array $values
   *   The argument array that contain dimensions values, should be an array of one more more values.
   *
   * @return string
   *   Returns the parameter value with tokens replaced by dimension values.
   */
  static public function dimensionValuesDisplayString($values, $separators = []) {
    // Use either the default separators or argument.
    $separators = $separators ?: self::$defaultSeparators;

    return implode($separators['term'], $values);
  }

  /**
   * Get the dimensions (list of vocabularies) in use for discovery.
   *
   * @return array
   *   The associative array of vocabularies keyed by machine_id of vocabulary.
   */
  public function getDiscoveryDimensionsVocabularies() {
    $dimensions = [];

    // Get the taxonomies marked for discovery in the configurations.
    $vocabularies_config = $this->configFactory
      ->get('f1_p13n.settings')
      ->get('taxonomy_dimensions');

    if (!empty($vocabularies_config)) {
      // Get the list of vocabularies currently defined on the site.
      $vocabularies = $this->vocabStorage->loadMultiple();

      // Check to see whether the dimension is defined as a vocabulary on the site. It should be.
      // If it is, record the machine ID and label of the vocabulary.
      foreach ($vocabularies_config as $possible_dimension) {
        if (isset($vocabularies[$possible_dimension])) {
          $dimensions[$possible_dimension] = $vocabularies[$possible_dimension]->label();
        }
      }
    }

    return $dimensions;
  }

  /**
   * Get the dimensions in use for discovery.
   *
   * @return array
   *   The associative array of dimensions keyed by dimension machine ID.
   */
  public function getDiscoveryDimensions() {
    $dimensions = $this->getDiscoveryDimensionsVocabularies();

    // Add in the defaults. Even if these are not being tracked through RDFa, we'll include them.
    $dimensions['page_type'] = $this->t('Page type');
    $dimensions['page_section'] = $this->t('Page section');

    return $dimensions;
  }

}
