<?php

namespace Drupal\f1_p13n\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * API endpoint to fetch personalization content.
 */
class PersonalizationContent extends ControllerBase {

  /**
   * The render service.
   *
   * @var Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor for personalization content fulfillment.
   *
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   */
  public function __construct(RendererInterface $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * Callback for `personalization/content` fulfillment API method.
   *
   * @param string $data
   *   The encoded string from client.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   A JSON response.
   *
   * @throws \Exception
   */
  public function fulfillExperience(string $data) {
    // Every call to fulfill and experience or experiences will result in a
    // returned response. The response structure is keyed by experience UUID
    // and includes an action (such as 'append') and a status.
    $response = [];

    // Decode url. This will result in an associative array whose key is the
    // UUID of the experience and whose array value is an array of dimensions
    // and values passed back from the user model.
    $p13nDivs = json_decode($this->base64UrlDecode($data), TRUE);

    // Pull the experience ids from the incoming experience containers through
    // the API call.
    $p13nExperienceIds = array_keys($p13nDivs);

    // For each experience id, iterate through each dimension.
    foreach ($p13nExperienceIds as $id) {
      // Decode the UUID.
      $uuid = base64_decode($id);

      // Create the default entry for this experience. The default is
      // "NOT FOUND" until proven otherwise. At present, only a single action
      // ("append") is supported.
      $response[$id] = [
        'uuid' => $uuid,
        'markupContent' => '',
        'action' => 'append',
        'status' => 404,
      ];

      // Retrieve the experience identified by the uuid if there is one.
      $experience = $this->entityTypeManager()->getStorage('f1_p13n_experience')->loadByUuid($uuid);

      // If no matching experience was found, return the 404 result.
      if ($experience == NULL) {
        continue;
      }

      // Extract the set of dimensions and their values from the user model via
      // the API call.
      $dimensions = array_keys($p13nDivs[$id]);

      // For each dimension, get top dimension value(s).
      foreach ($dimensions as $dimension) {
        $response[$id][$dimension] = $p13nDivs[$id][$dimension];
      }

      $rules = $experience->rules->view([
        'label' => 'hidden',
        'settings' => [
          // Pass dimensions for uuid to formatter.
          'experience_dimensions' => $response[$id],
        ],
      ]);

      $response[$id]['markupContent'] = $this->renderer->render($rules);
      $response[$id]['status'] = 200;
    }

    return new JsonResponse($response);
  }

  /**
   * Decode data from Base64URL.
   *
   * @param string $data
   *   The encoded string from client.
   * @param bool $strict
   *   The decode mode.
   *
   * @return bool|string
   *   The decoded string.
   */
  protected function base64UrlDecode($data, $strict = FALSE) {
    // Convert Base64URL to Base64 by replacing '-' with '+',
    // '_' with '/' and ',' with '='.
    $b64 = strtr($data, '-_,', '+/=');

    // Decode Base64 string and return the original data.
    return base64_decode($b64, $strict);
  }

}
