CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

Display content that matches the user's interests.


REQUIREMENTS
------------

This module requires:
* Drupal core >= 8.9


INSTALLATION
------------

Install the module as you would normally install a sandbox Drupal module.


Configuration
-------------

1. Configure RDFa tags.
2. Create Campaign at F1 Personalization -> Campaigns.
3. Select vacabularies you want to personalize for at F1 Personalization -> Dimension Settings.
4. Add experience field to content type (ex: Article). 
5. Create Article.
6. Add Experience field in layout builder. Ensure Experience Default formatter is selected.
7. If fulfillment is working, you should see text in the Experience field of the format "Markup for experienceId: base64 encoded pid".