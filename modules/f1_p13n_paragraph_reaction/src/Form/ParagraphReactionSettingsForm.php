<?php

namespace Drupal\f1_p13n_paragraph_reaction\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Paragraph reaction settings.
 *
 * @ingroup f1_p13n_paragraph_reaction
 *
 * @todo Create warning for paragraph reactions that are still in use
 * if the paragraph reaction is disabled when validating form settings.
 */
class ParagraphReactionSettingsForm extends ConfigFormBase {

  /**
   * The Paragraph reaction service.
   *
   * @var \Drupal\f1_p13n_paragraph_reaction\ParagraphReactionService
   */
  protected $paragraphReactionService;

  /**
   * The reaction plugin manager.
   *
   * @var \Drupal\f1_p13n\ExperiencePluginManager
   */
  protected $reactionManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->paragraphReactionService = $container->get('f1_p13n_paragraph_reaction');
    $instance->reactionManager = $container->get('plugin.manager.f1_p13n.reaction');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'f1_p13n_paragraph_reaction.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'f1_p13n_paragraph_reaction_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Retrieve the list of all defined paragraph bundles using the module's service.
    $available_bundles = $this->paragraphReactionService->getAllParagraphBundles();

    // Retrieve the list of bundles previously specified as allowed. It's possible that not all of the previously
    // allowed bundles will still exist, and new bundles may have been added.
    $config_bundles = $this->config('f1_p13n_paragraph_reaction.settings')->get('allowed_bundles') ?? [];

    // If no bundles were specified, all bundles are allowed. If some were specified, prune out of the default list
    // any bundles that are no longer still extant.
    foreach ($config_bundles as $bundle_id) {
      if (!isset($available_bundles[$bundle_id])) {
        unset($config_bundles[$bundle_id]);
      }
    }

    $form['paragraph_bundles'] = [
      '#type' => 'details',
      '#title' => $this->t('Components (Paragraph types) available for use in reactions'),
      '#description' => $this->t('If none are selected, all will be allowed.'),
      '#open' => TRUE,
    ];

    // Bundle selection form element.
    $form['paragraph_bundles']['allowed_bundles'] = [
      '#type' => 'checkboxes',
      '#options' => $available_bundles,
      '#default_value' => $config_bundles,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $enabled = [];

    // Retrieve the bundle list from the form. Value will be 0 if unselected.
    $selected = $form_state->getValue('allowed_bundles');
    foreach ($selected as $bundle_id => $value) {
      if ($value !== 0) {
        $enabled[] = $bundle_id;
      }
    }

    // Save into configuration. Note that if no bundles were selected, the configuration value
    // will be the empty array, signifying that all bundles can be used.
    $this->config('f1_p13n_paragraph_reaction.settings')
      ->set('allowed_bundles', $enabled)
      ->save();

    $this->reactionManager->clearCachedDefinitions();

    parent::submitForm($form, $form_state);
  }

}
