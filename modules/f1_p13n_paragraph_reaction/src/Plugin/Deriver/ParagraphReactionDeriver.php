<?php

namespace Drupal\f1_p13n_paragraph_reaction\Plugin\Deriver;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\ctools\Plugin\Deriver\EntityDeriverBase;
use Drupal\f1_p13n_paragraph_reaction\ParagraphReactionService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a reaction derivative for every paragraph type.
 */
class ParagraphReactionDeriver extends EntityDeriverBase {

  /**
   * The Paragraph reaction service.
   *
   * @var \Drupal\f1_p13n_paragraph_reaction\ParagraphReactionService
   */
  protected $paragraphReactionService;

  /**
   * Constructor.
   *
   * @param \Drupal\f1_p13n_paragraph_reaction\ParagraphReactionService $paragraph_reaction_service
   *   The ParagraphReaction service.
   */
  public function __construct(ParagraphReactionService $paragraph_reaction_service) {
    $this->paragraphReactionService = $paragraph_reaction_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('f1_p13n_paragraph_reaction')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = parent::getDerivativeDefinitions($base_plugin_definition);

    // Get the paragraph bundles selected by administration.
    $paragraph_bundles = $this->paragraphReactionService->getSelectedParagraphBundles();

    // Create a derivative reaction for each applicable paragraph bundle.
    foreach ($paragraph_bundles as $bundle => $label) {
      // Use the paragraph reaction plugin as the base, since the Paragraph reaction plugin will
      // be able to determine the paragraph bundle by getting the derivative ID.
      $this->derivatives[$bundle] = $base_plugin_definition;

      // see ExperienceForm::getReactionOptions for more information. Label is used to identify
      // reactions in the rule builder.
      $this->derivatives[$bundle]['label'] = $label;
    }

    return $this->derivatives;
  }

}
