<?php

namespace Drupal\f1_p13n_paragraph_reaction\Plugin\f1_p13n\Reaction;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Entity\EntityFormBuilderInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\f1_p13n\Reaction\ReactionPluginBase;
use Drupal\f1_p13n\TokenizationService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\inline_entity_form\Element\InlineEntityForm;

/**
 * Defines a reaction rendering a Paragraph field.
 *
 * @Reaction(
 *   id = "paragraph",
 *   label = @Translation("Paragraph"),
 *   deriver = "Drupal\f1_p13n_paragraph_reaction\Plugin\Deriver\ParagraphReactionDeriver",
 *   category = @Translation("Component"),
 * )
 */
class ParagraphReaction extends ReactionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity form builder.
   *
   * @var \Drupal\Core\Entity\EntityFormBuilderInterface
   */
  protected $entityFormBuilder;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityFormBuilderInterface $entity_form_builder
   *   The entity form builder service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      EntityTypeBundleInfoInterface $entity_type_bundle_info,
      EntityTypeManagerInterface $entity_manager,
      EntityFormBuilderInterface $entity_form_builder,
      RendererInterface $renderer,
      EntityFieldManagerInterface $entity_field_manager) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->entityTypeManager = $entity_manager;
    $this->entityFormBuilder = $entity_form_builder;
    $this->entityFieldManager = $entity_field_manager;
    $this->renderer = $renderer;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.bundle.info'),
      $container->get('entity_type.manager'),
      $container->get('entity.form_builder'),
      $container->get('renderer'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'pid' => '',
      'dimensions' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = [];

    // Set up the default form elements needed for downstream reaction processing and display.
    $form['#attributes']['class'][] = 'reaction';
    $form['dimensions'] = [
      '#type' => 'hidden',
      '#default_value' => $this->configuration['dimensions'],
    ];

    // The derivative id is the machine id of the paragraph type.
    $derivative = $this->getDerivativeId();

    // Retrieve the paragraph ID from configuration.
    $pid = $this->configuration['pid'];

    // Instantiate a paragraph entity of the correct type, either by loading the paragraph of the known pid
    // or creating a new one using the derivative to identify the paragraph type.
    if ($pid) {
      $paragraph = $this->entityTypeManager
        ->getStorage('paragraph')
        ->load($pid);
    }
    else {
      $paragraph = Paragraph::create([
        'type' => $derivative,
      ]);
    }

    // If there's no paragraph entity it means either that the configured pid was missing or a new one
    // could not be created for some reason.
    if (!$paragraph) {
      $form['error'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'index-changed',
            'messages',
            'messages--error',
          ],
        ],
        '#children' => $this->t("Component was not found or could not be created."),
        '#weight' => -10,
      ];
    }

    // Overload the pid form element to use an inline_entity_form.
    $form["ief_$derivative"] = [
      '#type' => 'inline_entity_form',
      '#entity_type' => 'paragraph',
      '#bundle' => $derivative,
      '#default_value' => $paragraph,
      '#form_mode' => 'default',
      '#save_entity' => true,
      '#element_validate' => [
        [get_class($this), 'iefElementValidation'],
      ]
    ];

    $form['pid'] = [
      '#type' => 'hidden',
      '#default_value' => $this->configuration['pid'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * A callback for validating the filteredView form.
   *
   * @param array $element
   *   The form element.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   The form state for the entire form.
   * @param array $form
   *   The entire form.
   */
  public static function iefElementValidation(array &$element, FormStateInterface $form_state, array &$form) {
    InlineEntityForm::validateEntityForm($element, $form_state);
    InlineEntityForm::submitEntityForm($element, $form_state);

    // Retrieve the paragraph that was created (or modified) through the inline editing form submission.
    $paragraph = $element['#entity'];
    $pid = $paragraph->id();

    $pid_parents = array_slice($element['#parents'], 0, -1);
    $pid_parents[] = 'pid';

    $form_state->setValue($pid_parents, $pid);

  }

  /**
   * {@inheritdoc}
   */
  public function view($dimensions = []) {
    // Retrieve the paragraph ID from configuration, and exit if empty.
    // If there is no paragraph Id in configuration, there will be nothing displayed.
    $pid =  $this->configuration['pid'];

    if (!$pid) {
      return [];
    }

    // Build a render array to contain the content markup passed back to the client.
    $build = [];

    // Load the paragraph and if found, render and return.
    $paragraph = $this->entityTypeManager->getStorage('paragraph')->load($pid);
    if ($paragraph) {
      // @todo Check published status of paragraph and user/role access as appropriate.
      //
      // Create a view builder to render the paragraph. Note assumption of default view mode.
      $view_builder = $this->entityTypeManager->getViewBuilder('paragraph');
      $element = $view_builder->view($paragraph, 'default', $paragraph->language()->getId());

      // Render the paragraph to markup and return if found.
      if ($rendered_element = $this->renderer->render($element)) {
        $build =  [
          '#theme' => 'p13n_reaction',
          '#plugin' => 'paragraph',
          '#tag' => 'div',
          '#content' => [
            '#type' => 'markup',
            '#markup' => $rendered_element,
          ],
        ];
      }
    }

    return $build;
  }

  /**
   * A callback for the onDelete event.
   *
   * See RuleService for more information.
   */
  public function onDelete() {
    // This ($this) is the reaction that is about to be deleted, so extract the paragraph id (pid) and delete it first.
    $configuration = $this->configuration;
    if (isset($configuration['pid'])) {
      $entity = $this->entityTypeManager->getStorage('paragraph')->load($configuration['pid']);
      if ($entity) {
        $entity->delete();
      }
    }
  }

  /**
   * A callback for the onCreate event.
   *
   * See RuleService for more information.
   */
  public function onCreate() {

  }

  /**
   * A callback for the onChange event.
   *
   * The reaction context (the pre-change state of the reaction) is available as an argument to the function.
   * To retrieve the context:
   *
   * $numargs = func_num_args();
   * if ($numargs >= 1) {
   *   // @var \Drupal\f1_p13n_paragraph_reaction\Plugin\f1_p13n\Reaction\ParagraphReaction $reaction
   *   $reaction = func_get_arg(0);
   * }
   *
   * See RuleService for more information.
   */
  public function onChange() {

  }

}
