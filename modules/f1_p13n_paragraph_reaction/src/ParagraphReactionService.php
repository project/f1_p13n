<?php

namespace Drupal\f1_p13n_paragraph_reaction;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;

/**
 * A class containing methods for services used by the Paragraph reaction.
 */
class ParagraphReactionService {

  /**
   * The entity type bundle info.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected $entityTypeBundleInfo;

  /**
   * Configuration settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $configFactory;

  /**
   * Constructs new EntityViewDeriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(EntityTypeBundleInfoInterface $entity_type_bundle_info, ConfigFactoryInterface $config_factory) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->configFactory = $config_factory;
  }

  /**
   * Returns a list of all Paragraph bundles, sorted alpha asc by label.
   *
   * @return array
   *   Array with allowed Paragraph bundles.
   */
  public function getAllParagraphBundles() {
    $bundles = $this->entityTypeBundleInfo->getBundleInfo('paragraph');

    // Enrich bundles with their label.
    foreach ($bundles as $bundle => $props) {
      $label = empty($props['label']) ? ucfirst($bundle) : $props['label'];
      $bundles[$bundle] = $label;
    }

    // Alpha sort ascending by label and return.
    asort($bundles, SORT_STRING);
    return $bundles;
  }

  /**
   * Returns a list of Paragraph bundles selected in administration, sorted alpha asc by label.
   *
   * @return array
   *   Array with allowed Paragraph bundles.
   */
  public function getSelectedParagraphBundles() {
    // Retrieve all bundles defined as of this moment.
    $all_bundles = $this->getAllParagraphBundles();

    // Retrieve the list of paragraph bundles selected by the administrator for use as reactions.
    // If the list (array) is empty, all paragraph bundles are indicated.
    $config_bundles = $this->configFactory->get('f1_p13n_paragraph_reaction.settings')->get('allowed_bundles');

    if (empty($config_bundles)) {
      return $all_bundles;
    }
    else {
      // Construct an options list comprised of the bundle IDs from administration with the current labels
      // associated with those paragraph types. Omit from the list any bundles that may have been deleted
      // since the last time the configuration list was edited.
      $bundles = [];
      foreach ($config_bundles as $bundle_id) {
        if (isset($all_bundles[$bundle_id])) {
          $bundles[$bundle_id] = $all_bundles[$bundle_id];
        }
      }

      return $bundles;
    }
  }

}
