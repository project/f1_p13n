CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration


INTRODUCTION
------------

The Personalization Paragraph Reaction module provides a plugin that enables content administrators
to use Drupal Paragraphs (see https://www.drupal.org/project/paragraphs) in personalization reactions.
The set of paragraph types (referred to as Components in the personalization rule builder) can be
configured in personalization settings.


REQUIREMENTS
------------

This module requires the following modules:

* F1 Personalization (f1_p13n)
* Paragraphs (https://www.drupal.org/project/paragraphs)
* Inline Entity Form (https://www.drupal.org/project/inline_entity_form)


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The set of Paragraphs that are available to personalization rule designer are specified in the
Personalization administration settings.

1. Navigate to Administration > Structure > Personalization > Personalization settings
2. Select which paragraph types to enable for use in reactions

